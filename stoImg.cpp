#include "GNFC.h"
#include <ARKE/Image.h>
#include <ARKE/AppData.h>
#include <ARKE/ImageFont.h>
#include "MDNode.h"
#include <signal.h>
#include <atomic>
//#define IMG
using namespace GNFC;


static bool paused = false;
namespace GNFC {

    void Pause() {
        paused = true;
        static char line[256];
        std::cin.getline(line, 256);
        std::cout << "GOT LINE " << line << "\n";
        if (line[0] == 'g') {
           
        } else if(line[0] == 'x') {
            exit(0);
        }
        
        paused = false;
    }
};

void sigINT(int sig) {
    sigset_t ss;
    sigemptyset(&ss);
    sigaddset(&ss, sig);
    sigprocmask(SIG_UNBLOCK, &ss, NULL);
    //pthread_sigmask(SIG_UNBLOCK, &ss, NULL);
    
    if(paused) exit(0);
    else GNFC::Pause();
}

int main(int argc, char** argv) {
    _unused(argc, argv);
    AppData::Initialize("./");
    OverrideTime();

    signal(SIGINT, sigINT);
    //ImageFont::Initialize();
    InvariantAxis red, green, blue;
    red.name = "red"; green.name = "green"; blue.name = "blue";
    red.range.set(0, 255);
    green.range.set(0, 255);
    blue.range.set(0, 255);
    
    red.range.increment = blue.range.increment = green.range.increment = 1;
    
    VariantAxis px, py;
    px.name = "x"; py.name = "y";
    
    blue.windowTime = green.windowTime = red.windowTime = px.windowTime = py.windowTime = 1000.0;
    /*
    space->add(red);
    space->add(green);
    space->add(blue);
    space->add(px);
    space->add(py);
*/
    auto *hdi = new MDNode<Vec, RecordedJacobian>();

    auto input = new ImageRGBA("oHDITest.png");
    //auto input = new ImageRGBA("hdiTest.png");
    
    unsigned int imgSf = 4;
    auto img = new ImageRGBA(input->width*imgSf, input->height*imgSf);
    auto simple = new ImageRGBA(input->width*imgSf, input->height*imgSf);
    auto meta = new ImageRGBA(input->width*imgSf, input->height*imgSf);
    
    px.range.set(0, input->width);
    py.range.set(0, input->height);
    px.range.increment = py.range.increment = 1;
    px.windowTime = py.windowTime = 1000.0;
    
    hdi->origin[&px] = 0.0;
    hdi->origin[&py] = 0.0;
    hdi->origin[&red] = hdi->origin[&blue] = hdi->origin[&green] = 0;
    hdi->span[&px] = input->width;
    hdi->span[&py] = input->height;
    hdi->span[&red] = hdi->span[&blue] = hdi->span[&green] = 255;
    
    const unsigned int totalPass = 8;
    std::atomic<unsigned int> inCnt;
    inCnt = 0;
    
    for(unsigned int pass = 0; pass < totalPass; pass++) {
#pragma omp parallel for
        for(unsigned int x = 0; x < input->width; x++) {
            for(unsigned int y = 0; y < input->height; y++) {
                const ColorRGBA& pix = input->getPixel(x,y);

                Vec pos, col;
                pos[&px] = x;
                pos[&py] = y;

                col[&red] = pix.r;
                col[&green] = pix.g;
                col[&blue] = pix.b;

                hdi->insert(pos, col, 1.0);
                StepTime(0.001);
                //usleep(1);
                //std::cout << "\tinserted " << x << ", " << y << "\n";

                //if(y > 100) break;
            }
            inCnt++;
            std::cout << round((double(inCnt)/double(input->width*totalPass))*10000.0)/100.0 << "% complete               \r";
            std::cout.flush();
            //break;
        }
        std::cout << "\nPass " << pass << " completed\n";
    }
    
    std::fstream fs;
    fs.open ("graph.dot", std::fstream::in | std::fstream::out | std::fstream::trunc);
    hdi->debugGraph(fs);
    fs.close();
    
    //circo
    //std::string cmd = "circo -Tpng graph.dot -o imgTest.png";
    //system(cmd.c_str());
    
    unsigned int maxDepth = 0;
    std::atomic<unsigned int> outCnt;
    outCnt = 0;
    #pragma omp parallel for
    for(unsigned int x = 0; x < img->width; x++) {
        for(unsigned int y = 0; y < img->height; y++) {
            ColorRGBA& pix = img->getPixelRef(x,y);
            
            Vec pos, col;
            pos[&px] = double(x) / imgSf;
            pos[&py] = double(y) / imgSf;
            
            unsigned int depth = 0; double dists;
            Vec qCol = hdi->query(pos, &depth, &dists);
            //auto node = hdi->resolveMDN(pos);
            col = hdi->sample(pos, pos);
            //std::cout << "[" << x << "," << y << "]=" << col << "\n";
            pix.r = col[&red];
            pix.g = col[&green];
            pix.b = col[&blue];
            pix.alphaRef() = 255;
            
            ColorRGBA& qpix = simple->getPixelRef(x,y);
            qpix.r = qCol[&red];
            qpix.g = qCol[&green];
            qpix.b = qCol[&blue];
            
            ColorRGBA& mpix = meta->getPixelRef(x,y);
            mpix.r = depth * 16.0;
            mpix.g = dists * 128.0;
            mpix.b = 0;//node->value.history.variance(100.0).relDistanceTo(Vec()) * 32.0 + 127.0;
            mpix.g = mpix.b = mpix.r;
            mpix.alphaRef() = 255;
            maxDepth = std::max(depth, maxDepth);
        }
        outCnt++;
        std::cout << round((double(outCnt)/double(img->width))*10000.0)/100.0 << "% complete [image]                 \r";
        std::cout.flush();
    }
    std::cout << "\n\nDone, with a max depth of " << maxDepth << "\n";
        //space->update();
        
        //space->debugPrint();

    img->repAlpha(0, 255);
    img->Save("hdiOut.png");
    meta->repAlpha(0, 255);
    meta->Save("hdiMeta.png");
    simple->repAlpha(0, 255);
    simple->Save("hdiRaw.png");
    return 0;
}
