NAME = GNFCtrl
SYS_NAME = GNFCtrl

#Directory that contains .cpp files with main()
EXEC_DIRS = .

#directories containing headers that need to be installed
HEADER_DIRS = space testing data stem directives intrin
#Header install subdirectory (ie, in /usr/include: defaults to $(SYS_NAME))
HEADERS_OUT_DIR = GNFC/

#Choose ONE header, if any, to precompile and cache (not for developement!!!)
PCH =

#Default platform
TARGET_PLATFORM ?= Desktop
#Local build output directory
BUILD_DIR = build

#Compiler
CXX ?= g++
#CFLAGS (appended to required ones)
CXXPLUS ?= -fopenmp
#-fuse-ld=gold 

#SYS_FLAGS (prefix and possible override system CFLAGS, may break things)
CC_SYS_FLAGS ?=
#Optimization flags, supporting PGO if needed
OPTI := -Ofast
#Include paths, ie -I/path/to/headers/
INCPATH += `freetype-config --cflags`
#Libraries, ie -lopenmp
LIBS += -Wl,-Bstatic -Wl,-Bdynamic -lfreetype -lpthread -lboost_system  -lboost_filesystem 
LIBS += -lLARKE -lSpinster

include /usr/include/LPBT.mk

#Extra commands to be run on each target, add commands as you please (will be run after Buildsys ones)
install::

uninstall::

