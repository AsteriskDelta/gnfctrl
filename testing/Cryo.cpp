#include "Cryo.h"

static double curTemp = 110.0, curPressure = 10.0;
static Heater *heat; static Cooler *cool; static Pump *pump;

Heater::Heater() : VariantAxis() {
    Axis::range.set(0.0, 5.0);
    Axis::setName("Heater");
    Axis::range.dead = 0.00003;
    Axis::range.cutoff = 0.0001;
    heat = this;
}

Cooler::Cooler() : VariantAxis() {
    Axis::range.set(0.0, 30.0);
    Axis::setName("Cooler");
    cool = this;
}
Pump::Pump() : VariantAxis() {
    Axis::range.set(-10.0, 10.0);
    Axis::setName("Pump");
    pump = this;
}


Therm::Therm() : InvariantAxis() {
    Axis::setName("Temp");
    Axis::range.set(1.0, 450.0);
}
Pressure::Pressure() : InvariantAxis() {
    Axis::setName("Pressure");
    Axis::range.set(0.0, 28.0);
}

bool Therm::update() {
    this->read(curTemp);
    return Axis::update();
}
bool Pressure::update() {
    this->read(curPressure);
    return Axis::update();
}

void CryoSim(double dT) {
    double scale = 1.0;
    double del = heat->get() - cool->get()/4.0;
    curPressure = std::max(0.2, std::min(28.0, curPressure + pump->get() * dT * scale));
    
    del = del / (curPressure/2.0);
    
    static double oldDel = 0.0;
    del = del * 0.2 + oldDel * 0.8;
    curTemp += dT * scale * del;
    oldDel = del;
    //curTemp += -dT * 2.0;//(double(abs(rand())%2000)/2000.0 - 1.0) * dT * 0.5;
    
    curTemp = std::min(std::max(1.0, curTemp), 450.0);
}