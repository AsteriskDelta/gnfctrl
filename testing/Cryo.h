#ifndef CRYO_H
#define CRYO_H
#include "GNFC.h"
using namespace GNFC;

struct Heater : VariantAxis {
    Heater();
};

struct Cooler : VariantAxis {
    Cooler();
};
struct Pump : VariantAxis {
    Pump();
};

struct Therm : InvariantAxis {
    Therm();
    
    virtual bool update() override;
};
struct Pressure : InvariantAxis {
    Pressure();
    
    virtual bool update() override;
};

void CryoSim(double dT);

#endif /* CRYO_H */

