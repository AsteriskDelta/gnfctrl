#ifndef GNFINCLUDE_H
#define GNFINCLUDE_H
#include <string>
#include <cstdlib>
#include <unistd.h>
#include <iostream>
#include <cmath>
#include <Spinster/SpinsterInclude.h>

//#define _unused(x) ((void)x)


namespace GNFC {
    using namespace Spinster;
    
    namespace $ = ::GNFC;
    
    typedef double Num;
    class Axis;
    class Vec;
    class Space;
    class Path;
    class Planner;
    class Controller;
    class Directive;
    class Jacobian;
    class RecordedJacobian;
    class VariantAxis;
    class InvariantAxis;
    class Sequence;
    class SequenceInstance;
    class Explore;
    class UserControl;
    class PathError;
    
    template<typename VEC, typename VAL>
    class MDNode;
    
    typedef MDNode<Vec, RecordedJacobian> MDTree;
    
    void Pause() __attribute((weak));
    
    
    template<typename T> inline auto max(const T& a, const T& b);
    template<typename T> inline auto max(const T& a, const T& b) {
        return ::std::max(a, b);
    }
    template<typename T> inline auto min(const T& a, const T& b);
    template<typename T> inline auto min(const T& a, const T& b) {
        return ::std::min(a, b);
    }
}

#endif /* GNFINCLUDE_H */

