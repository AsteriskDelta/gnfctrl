#include "PathError.h"
#include "Controller.h"
#include "Path.h"
#include "Space.h"

namespace GNFC {

    PathError::PathError(Controller *const& ctrl) {
        Axis::range.set(0.0, 100.0);
        Axis::setName("PathError");
        controller = ctrl;
    }

    PathError::~PathError() {
    }
    
    bool PathError::update() {
        //if(!controller->path()) this->read(controller->path().err());
        //else if(controller->path().length <= 0.0 && controller->path().err() > 0.0) this->read(controller->path().err());
        //else this->read(controller->path().err() / controller->path().length);
        this->read(controller->space->target[this]);
        //std::cout << "Got path error of " << this->value << " from err=" << controller->path().err()  << ", len=" << controller->path().length << "\n";
        return Axis::update();
    }

};