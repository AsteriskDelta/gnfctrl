#ifndef PATHERROR_H
#define PATHERROR_H
#include "GNFInclude.h"
#include "Axis.h"

namespace GNFC {

    
    class PathError : public InvariantAxis, public HiddenAxis {
    public:
        PathError(Controller *const& ctrl);
        virtual ~PathError();
        
        virtual bool update() override;
        
    private:
        Controller *controller;
    };
};

#endif /* PATHERROR_H */

