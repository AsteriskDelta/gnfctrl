#ifndef SPACE_H
#define SPACE_H
#include "GNFInclude.h"
#include "Point.h"
#include "Jacobian.h"
#include <unordered_set>
#include "MDNode.h"

namespace GNFC {

    class Space {
    public:
        Space();
        //Space(const Space& orig);
        virtual ~Space();
        
        void add(Axis *axis);
        //bool update(const Num& dT);
        void read(const Num& dT);
        void write(const Num& dT);
        
        Vec point() const;
        
        void debugPrint();
        
        void setTarget(Vec pt);
        
        Vec calculateVariants(Vec delta, Path& resultPath);
        
        std::list<Jacobian> controls;
        std::unordered_set<Axis*> axes;
        Vec current, target, deriv, integ, variance;
        
        MDNode<Vec, RecordedJacobian> *hdi;
        Controller *controller;
        Num deltaTime;
    private:
        
    };

};

#endif /* SPACE_H */

