#ifndef AXIS_H
#define AXIS_H
#include "GNFInclude.h"
#include "History.h"

namespace GNFC {

    class Axis : public History <Num,Num> {
    public:
        Axis();
        Axis(const Axis& orig);
        virtual ~Axis();
        
        virtual bool update();
        virtual Num get() const;
        virtual bool set(Num targ);
        
        inline virtual bool variant() const { return false; };
        inline virtual bool hidden() const { return false; };
        void setName(const std::string& nn);
        
        std::string typeString() const;
        
        struct Range {
            Num min, max;
            Num dead = 0.0, last = 0.0;
            Num cutoff = 0.0;
            Num increment = 0.0;
                    
            void set(Num mnn, Num mxx);
            Num normalize(Num val, bool raw = false);
            
            inline Num operator()() const {
                return max - min;
            }
        };
        Range range;
        std::string name;
        
        bool canSubdivide(Num span);//Check if our increment (ie, finite step size) is small enough to subdivide
        
        Num windowTime;
    protected:
        Num value, target;
        void read(Num newValue);
    };
    
    struct VariantAxis : virtual Axis {
    public:
        inline VariantAxis() : Axis() {};
        inline virtual bool variant() const override { return true; };
    };
    struct InvariantAxis : virtual Axis {
        
        inline InvariantAxis() : Axis() {};
        inline virtual bool variant() const override { return false; };
    };
    struct HiddenAxis : virtual Axis {
        inline HiddenAxis() : Axis() {};
        inline virtual bool hidden() const override { return true; };
    };
}

#endif /* AXIS_H */

