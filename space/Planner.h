#ifndef PLANNER_H
#define PLANNER_H
#include "GNFInclude.h"
#include "Point.h"
#include "Path.h"

namespace GNFC {

    class Planner {
    public:
        Planner(Space *sp, MDTree *tr);
        virtual ~Planner();
        
        Space *space;
        MDTree *hdi;
        
        Vec evaluate(const Vec& current);
        
        void setTarget(const Vec& targ);
        
        Path plan(const Vec& from, const Vec& to);
        Vec naive(const Vec& from, const Vec& to);
        
        inline Path& path() {
            return cache.path;
        }
    protected:
        Path plan(const Vec& from, const Vec& to, std::priority_queue<Path> & paths);
        
        Vec target, lastTarget;
        struct {
            Path path;
        } cache;
    };
};

#endif /* PLANNER_H */

