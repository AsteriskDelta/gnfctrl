#include "Point.h"
#include "Axis.h"
#include <sstream>

namespace GNFC {

    Vec::Vec() : data(), map(), defaultValue(0.0) {
    }
    Vec::Vec(const Value& def) : data(), map(), defaultValue(def) {
        
    }

    Vec::Vec(const Vec& orig) : data(), map(), defaultValue(orig.defaultValue) {
        for(auto it = orig.data.begin(); it != orig.data.end(); ++it) {
            (*this)[it->axis] = it->value;
        }
    }

    Vec::~Vec() {
    }
    
    Vec& Vec::operator=(const Vec& o) {
        this->clear();
        for(const AxisData& dat : o.data) {
            (*this)[dat.axis] = dat.value;
        }
        return *this;
    }
    
    bool Vec::AxisData::matches(DataType type) const {
        switch(type) {
            case Variant:
                return axis->variant();
            case Invariant:
                return !axis->variant();
            case Hidden:
                return axis->hidden();
            default: [[fallthrough]];
            case Invalid:
                return false;
                break;
        }
    }
    
    Num& Vec::get(Axis* axis, bool ins) {
        if(map.find(axis) == map.end()) {
            if(!ins) return defaultValue;
            
            data.push_back(AxisData{axis, defaultValue});
            map[axis] = std::prev(data.end());
        }
        
        return map[axis]->value;
    }

    bool Vec::remove(Axis *axis) {
        if(map.find(axis) == map.end()) {
            return false;
        }
        data.erase(map[axis]);
        map.erase(axis);
        return true;
    }
    
    void Vec::readAxes() {
        for(AxisData& dat : data) {
            dat.value = dat.axis->get();
        }
    }
    void Vec::writeAxes() {
        for(AxisData& dat : data) {
            dat.axis->set(dat.value);
        }
    }
    
    Vec Vec::filter(DataType type, bool keep) const {
        Vec ret;
        for(const AxisData &dat : data) {
            if(dat.matches(type) == keep) ret[dat.axis] = dat.value;
        }
        return ret;
    }
    Vec Vec::filter(const Vec& o, bool keep) const {
        Vec ret;
        for(const AxisData &dat : data) {
            if(o.has(dat.axis) == keep) ret[dat.axis] = dat.value;
        }
        return ret;
    }
    Vec Vec::filter(Vec::AxisType *const& axis, bool keep) const {
        if(keep) {
            Vec ret;
            ret[axis] = (*this)[axis];
            return ret;
        } else {
            Vec ret = *this;
            ret.remove(axis);
            return ret;
        }
    }
    
    Num Vec::magnitude2() const {
        Num ret = 0.0;
        for(const AxisData &dat : data) {
            ret += dat.value*dat.value;
        }
        return ret;
    }
    Num Vec::magnitude() const {
        const auto mg2 = this->magnitude2();
        if(mg2 == 0.0) return 0.0;
        else return sqrt(mg2);
    }
    Num Vec::distanceTo(const Vec &o) const {
        return (const_cast<Vec&>(o) - *this).magnitude();
    }
    Num Vec::distanceTo2(const Vec &o) const {
        return (const_cast<Vec&>(o) - *this).magnitude2();
    }
    
    Num Vec::relDistanceTo(const Vec &o) const {
        return sqrt(this->relDistanceTo2(o));
    }
    Num Vec::relDistanceTo2(const Vec &o) const {
        Vec del = o - *this;
        del.makeRelative(true);
        return del.magnitude2();
    }
    
    Num Vec::dot(const Vec& ot) const {
        Vec &o = const_cast<Vec&>(ot);
        Num ret = 0;
        for(const AxisData &dat : data) {
            if(!o.has(dat.axis)) continue;
            else ret += dat.value * o[dat.axis];
        }
        return ret;
    }
    
    Vec Vec::nonZero() const {
        Vec ret;
        for(const AxisData &dat : data) {
            if(fabs(dat.value) > 0.001) ret[dat.axis] = dat.value;
        }
        return ret;
    }
    
    Vec& Vec::makeRelative(bool raw) {
        for(AxisData &dat : data) {
            //std::cout << "\t\tmake rel " << dat.axis->name << " val=" << dat.value << 
            //        " on range " << dat.axis->range.min << " ... " << dat.axis->range.max << "\n";
            dat.value = dat.axis->range.normalize(dat.value, raw);
            //std::cout << "\t\t\tgot " << dat.value << "\n";
        }
        return *this;
    }
    
    Vec& Vec::clamp() {
        for(AxisData &dat : data) {
            if(dat.value > dat.axis->range.max) dat.value = dat.axis->range.max;
            else if(dat.value < dat.axis->range.min) dat.value = dat.axis->range.min;
        }
        return *this;
    }
        
    Vec Vec::operator-(const Vec& o) const {
        Vec ret;
        for(const AxisData &dat : data) {
            ret[dat.axis] = dat.value - o[dat.axis];
        }
        for(const AxisData &odat : o.data) {
            if(!has(odat.axis)) {
                ret[odat.axis] = -odat.value;
            }
        }
        return ret;
    }
    Vec Vec::operator+(const Vec& o) const {
        Vec ret;
        for(const AxisData &dat : data) {
            ret[dat.axis] = dat.value + o[dat.axis];
        }
        for(const AxisData &odat : o.data) {
            if(!has(odat.axis)) {
                ret[odat.axis] = odat.value;
            }
        }
        return ret;
    }
    
    Vec Vec::operator*(const Vec& o) const {
        Vec ret;
        for(const AxisData &dat : data) {
            if(!o.has(dat.axis)) continue;
            ret[dat.axis] = dat.value * o[dat.axis];
        }
        return ret;
    }
    Vec Vec::operator/(const Vec& o) const {
        Vec ret;
        for(const AxisData &dat : data) {
            if(!o.has(dat.axis) || o[dat.axis] == 0.0) continue;
            ret[dat.axis] = dat.value / o[dat.axis];
        }
        return ret;
    }
    
    Vec Vec::operator*(const Num& v) const {
        Vec ret;
        for(const AxisData &dat : data) {
            ret[dat.axis] = dat.value * v;
        }
        return ret;
    }
    Vec Vec::operator/(const Num& v) const {
        Vec ret;
        for(const AxisData &dat : data) {
            ret[dat.axis] = dat.value / v;
        }
        return ret;
    }
    
    Vec Vec::operator|(const Vec& o) const {
        Vec ret(*this);
        for(const AxisData &odat : o.data) {
            if(!ret.has(odat.axis)) {
                ret[odat.axis] = odat.value;
            }
        }
        return ret;
    }
    Vec Vec::operator&(const Vec& o) const {
        Vec ret;
        for(const AxisData &dat : data) {
            if(o.has(dat.axis)) {
                ret[dat.axis] = dat.value;
            }
        }
        return ret;
    }
    
    bool Vec::operator==(const Vec& o)  const {
        return (*this - o).magnitude() < 0.0001;
    }
    
    bool Vec::operator<(const Vec& o) const {
        //for(const AxisData &odat : this->data) if((*this)[odat.axis] >= o[odat.axis]) return false;
        //for(const AxisData &odat : o.data) if((*this)[odat.axis] >= o[odat.axis]) return false;
        for(const AxisData &dat : data) {
            if(!o.has(dat.axis)) continue;
            if(o[dat.axis] <= dat.value) return false;
        }
        return true;
    }
    bool Vec::operator>=(const Vec& o) const {
        for(const AxisData &dat : data) {
            if(!o.has(dat.axis)) continue;
            if(o[dat.axis] > dat.value) {
                //std::cout << "\t\t >= FAILED due to " << o[dat.axis]<<" > "<<dat.value<<"\n";
                return false;
            }
        }
        return true;
    }
    bool Vec::operator<=(const Vec& o) const {
        for(const AxisData &dat : data) {
            if(!o.has(dat.axis)) continue;
            if(o[dat.axis] < dat.value) {
                //std::cout << "\t\t <= FAILED due to " << o[dat.axis]<<" < "<<dat.value<<"\n";
                return false;
            }
        }
        return true;
    }
    
    bool Vec::operator>(const Num& o) const {
        for(const AxisData &dat : this->data) if((*this)[dat.axis] <= o) return false;
        return true;
    }
    bool Vec::operator>=(const Num& o) const {
        for(const AxisData &dat : this->data) if((*this)[dat.axis] < o) return false;
        return true;
    }
    
    bool Vec::has(Axis *axis) const {
        return map.find(axis) != map.end();
    }
    
    void Vec::clear() {
        map.clear();
        data.clear();
    }
    void Vec::debugPrint(const std::string& prefix) const {
        for(const auto& dat : data) {
            std::cout << prefix << dat.axis->name << " = " << dat.value << "\n";
        }
    }
    std::string Vec::str() const {
        std::stringstream ss;
        ss << "Vec(";
        for(auto& dat : data) {
            ss << dat.axis->name << " = " << dat.value << ", ";
        }
        ss << ")";
        return ss.str();
    }
    std::string Vec::iden(const Vec& to) const {
        std::stringstream ss;
        //ss << "v";
        for(auto it = data.begin(); it != data.end(); ++it) {
            //if(it != data.begin()) ss << "_";
            ss << it->axis->name  << " = " << it->value;
            if(to.has(it->axis)) ss << " -> " << to[it->axis];
            ss << "\\n";
        }
        //ss << ")";
        return ss.str();
    }
};