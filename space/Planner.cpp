#include "Planner.h"
#include "MDNode.h"
#include "Jacobian.h"
#include "Space.h"
#include <queue>

namespace GNFC {

    Planner::Planner(Space *sp, MDTree *tr) : space(sp), hdi(tr) {
    }

    Planner::~Planner() {
    }

    Vec Planner::evaluate(const Vec& current) {
        if(cache.path.complete()) {
            return cache.path.evaluate(current.filter(Vec::Hidden, false));
        } else {
            return this->naive(current.filter(Vec::Hidden, false), target);
        }
    }
    
    void Planner::setTarget(const Vec& targ) {
        if(targ == target) return;
        
        target = targ.filter(Vec::Hidden, false);
        Vec variants = target.filter(Vec::Variant);
        //target = target.filter(variants, false);//Remove variants from pathfinding
        
        cache.path.clear();
        cache.path = this->plan(variants | space->current.filter(Vec::Hidden, false), target);
        //cache.path |= variants;
        std::cout << "Got path length=" << cache.path.length << ", err=" << cache.path.error << ", eta=" << cache.path.eta << " as " << cache.path << "\n";
        //cache.path = ret;
        //Pause();
        
        //cache.path |= variants;
    }
    
    Path Planner::plan(const Vec& from, const Vec& to) {
        if(!to.filter(Vec::Invariant)) {
            Path ret(to);
            ret.error = 0;
            ret.length = 0;
            return ret;
        }
        //Path ret;
        std::priority_queue<Path> paths;
        paths.push(Path(from.filter(to.filter(Vec::Variant), false) | to.filter(Vec::Variant)));
        //Num bestTime = std::numeric_limits<Num>::max();
        std::cout << "PATHING from " << from << " -> " << to << "\n";
        Path ret = this->plan(from, to, paths);
        ret.error = to.filter(Vec::Invariant).distanceTo(ret.endPt().filter(Vec::Invariant));
        if(ret.error > 0.5 && ret.length == 0.0) ret.length = std::numeric_limits<Num>::max();
        else if(ret.length > 0.0) {
            ret.error /= ret.length;
        }
        //std::cout << "GOT PATH sz=" << ret.length << " = " << ret << "\n";
        //ret
        return ret;
    }
    Path Planner::plan(const Vec& from, const Vec& to, std::priority_queue<Path> & paths) {
        unsigned int tries = 0;
        Path best = paths.top();//(from | to.filter(Vec::Variant));
        while(!paths.empty() && (tries++) < 10000) {
            Path path = paths.top();
            paths.pop();
            
            Vec del = hdi->query(path.endPt()).filter(Vec::Invariant);//hdi->sample(path.endPt()).filter(Vec::Invariant);
            Vec desired = (to - path.endPt()).filter(Vec::Invariant);
            
            auto preIter = hdi->nearest(path.endPt(), space->point().filter(Vec::Variant));
            Vec dPreIter = (preIter->absolute - path.endPt()).filter(Vec::Invariant);
            
            if(del.dot(dPreIter) < 0.0/* || del.dot(desired) < 0.0*/ || del.magnitude2() <= 0.001) continue;
            
            Num dTime = (dPreIter / del).magnitude();
            
            //std::cout << "Path at " << path.endPt() << " with del=" << del << " and desired=" << desired << " has dTime=" << dTime << "\n";
            //Pause();
            
            //if(dTime > 1000.0) continue;
            dTime = std::max(0.1, dTime);
            
            Vec oldPos = path.endPt();
            path.next(path.endPt());
            path.endPt() += del * dTime;
            //std::cout << "path went from " << oldPos.filter(Vec::Invariant) << " to " << path.endPt().filter(Vec::Invariant) << "\n";
            
            path.length += dTime;
            if(path.length > 60.0) continue;//FOR DEV ONLY
            
            path.error = to.filter(Vec::Invariant).distanceTo(path.endPt().filter(Vec::Invariant));
            Num proj = (desired / del).magnitude();
            path.eta = proj;
            if(del.dot(desired) <= 0.0) proj = path.eta = std::numeric_limits<Num>::max();
            
            //if(path.eta < best.eta) best = path;
            if(path.error < best.error) best = path;
            //std::cout << "Path score=" << path.composite() << ", time=" << path.length << " err=" << path.error << " projected=" << proj << " = " << path << "\n";
            if(path.endPt().distanceTo2(to) < 0.8) break;//return path;
            
            
            //Pause();
            
            auto iter = hdi->nearest(path.endPt(), space->point().filter(Vec::Variant));
            unsigned int checks = 0;
            
            while(iter && (checks++) < 100) {
                //std::cout << "\tnearest iter gave " << iter->absolute << "\n";
                Path newPath = path;
                //newPath.next(path.endPt().filter(Vec::Invariant)  | iter->absolute.filter(Vec::Variant));
                newPath.endPt() = newPath.endPt().filter(Vec::Invariant) | iter->absolute.filter(Vec::Variant);
                newPath.endPt() = newPath.endPt().filter(to.filter(Vec::Variant), false) | to.filter(Vec::Variant);
                paths.push(newPath);
                ++iter;
            }
            /*
            Vec delta = (to - path.endPt()).filter(Vec::Invariant);
            if(delta.magnitude2() < 0.01) return path;
            
            auto iter = hdi->nearest(path.endPt(), space->point().filter(Vec::Variant));
            unsigned int checks = 0;

            while(iter && checks < 100) {
                Path child = path;
                
                Num iterTime = delta / *iter;
                Vec newPt = path.endPt() + *iter * iterTime;
                
                child.next(newPt);
                child.error = (to - newPt).magnitude();
                child.length += iterTime;
                ++iter;
                ++checks;
            }*/
            
            //if(paths.empty()) return path;
        }
        
        /*unsigned int checks = 0;
        
        while(iter && checks < 100) {
            
            ++iter;
            ++checks;
        }*/
        
        //Path ret = Path(from);
        //ret.error = std::numeric_limits<Num>::max();
        return best;//ret;
    }
    /*
    Path& Planner::plan(const Vec& from, const Vec& to, Path& path, Num& bestTime) {
        if(!to) return path;
        
        Vec delta = (to - from).filter(to), nextTarget;
        
        std::cout << "\t\tpathing from " << from  << " -> " << to << ", bestTime=" << bestTime << "\n";
        
        auto iter = hdi->nearest(from.filter(to));
        Num totalWeight = 0;//, maxDistance = std::numeric_limits<Num>::max();
        unsigned int checks = 0;
        bool improved = false;
        
        while(iter && checks < 100) {
            Vec& iterDeriv = *iter;
            Num dot = delta.dot(iterDeriv), dotNorm = 0.0;
            
            
            Vec baseDelta = (iter->absolute - iter.start->value.absolute).filter(to);
            Vec startAbsInvariant = iter.start->value.filter(to);//.absolute.filter(Vec::Variant);
            Num baseDotNorm = startAbsInvariant.dot(baseDelta) / (startAbsInvariant.magnitude() * baseDelta.magnitude());
            
            std::cout << "\t\t\t\tcheck pt " << iter->absolute << " with dot=" << dot << " and baseDot=" << baseDotNorm << "\n";
            
            if(dot > 0 && baseDotNorm > 0) {
                Num denom = (delta.filter(iterDeriv).magnitude() * iterDeriv.magnitude());
                if(denom > 0.0) dotNorm = dot / denom;

                //Num distance = from.distanceTo(iter->absolute);
                Num baseTime = (baseDelta / from).magnitude();
            
                Vec iterDelta = (to - from).filter(Vec::Invariant);
                
                Num estTime = (iterDelta / *iter).magnitude() + baseTime;//In seconds, upper bound
                
                //Weight by the alignment of the vectors to a desirable state
                estTime *= (log(1.0 / (dotNorm * baseDotNorm)) + 1.0);
                std::cout << "\t\t\tconsider " << iter->absolute << " for path with estTime=" << estTime << "\n";
                if(estTime < bestTime) {
                    improved = true;
                    bestTime = estTime;
                }
                
                const Num weight = (estTime == 0.0)? 1.0 / 0.0001 : 1.0 / estTime;
                
                nextTarget = nextTarget + iter->absolute * weight;
                totalWeight += weight;
            }

            ++checks;
            ++iter;
        }
        
        //If we reached a dead end, backpedal
        if(!improved) {
            path.scoreTo(to);
            std::cout << "\tPath ended, err=" << path.error << ", len=" << path.length << "\n";
            return path;
        }
        
        nextTarget = nextTarget / totalWeight;
        path.next(nextTarget | from);
        
        //Otherwise, continue from our current point
        return this->plan(nextTarget, to, path, bestTime);
    }
    */
    Vec Planner::naive(const Vec& from, const Vec& to) {
        
    }
};