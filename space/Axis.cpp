#include "Axis.h"

namespace GNFC {

    Axis::Axis() : History(), range(), name("?"), windowTime(3.0), value(0.0), target(0.0) {
        History::setDuration(1000.0);
    }

    Axis::Axis(const Axis& orig) : History(orig), range(orig.range), name(orig.name), windowTime(orig.windowTime), 
            value(orig.value), target(orig.target) {
    }

    Axis::~Axis() {
    }

    bool Axis::update() {
        if(range.dead == 0.0 && this->variant()) this->value = range.last = this->target;
        else if(this->variant()) {
            if(std::fabs(target - range.last) > range.dead) {
                this->value = range.last = target;
            }
        }
        
        if(range.cutoff > fabs(value) && value != 0.0) value = range.last = 0.0;
        
        //std::cout << "adding history for " << this->name << " = " << this->get() << "\n";
        this->add(this->get());
        return true;
    }

    Num Axis::get() const {
        if(range.increment == 0.0) return value;
        else return round(value / range.increment) * range.increment;
    }

    bool Axis::set(Num targ) {
        //std::cout << "AXIS " << this->name << " set to " << targ << "\n";
        if(!variant()) return false;
        else target = std::max(std::min(range.max, targ), range.min);
        
        if(range.increment != 0.0) target = round(target / range.increment) * range.increment;
        
        return true;
    }

    void Axis::read(Num newValue) {
        range.last = value = std::max(range.min, std::min(range.max, newValue));
    }
    
    void Axis::Range::set(Num mnn, Num mxx) {
        min = mnn;
        max = mxx;
    }
    Num Axis::Range::normalize(Num val, bool raw) {
        if(raw) return (val) / (max - min);
        else return (val - min) / (max - min);
    }
    
    void Axis::setName(const std::string& nn) {
        name = nn;
    }
    
    bool Axis::canSubdivide(Num span) {//Check if our increment (ie, finite step size) is small enough to subdivide
        return span >= range.increment * 2;
    }
    
    std::string Axis::typeString() const {
        std::string ret = "";
        if(this->hidden()) ret += "hidden ";
        
        if(this->variant()) ret += "out";
        else ret += "in";
        
        return ret;
    }
};

