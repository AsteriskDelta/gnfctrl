#include "Jacobian.h"

namespace GNFC {

    Jacobian::Jacobian() {
    }

    Jacobian::Jacobian(const Jacobian& orig) : Vec(orig), absolute(orig.absolute) {
    }

    Jacobian::~Jacobian() {
    }

    void Jacobian::clear() {
        Vec::clear();
        absolute.clear();
    }
};