#include "Space.h"
#include "Axis.h"
#include "Point.h"
#include "Path.h"
#include <queue>
#include "Controller.h"

namespace GNFC {

    Space::Space() : controller(nullptr) {
    }
/*
    Space::Space(const Space& orig) {
    }
*/
    Space::~Space() {
    }
    
    void Space::add(Axis *axis) {
        axes.insert(axis);
        current[axis] = 0;
        hdi->span[axis] = axis->range.max - axis->range.min;
        hdi->origin[axis] = axis->range.min;
        hdi->value.absolute[axis] = hdi->span[axis] / 2.0 + hdi->origin[axis];
    }
    /*bool Space::update(const Num& dT) {
        //deriv.clear(); integ.clear();
        for(Axis *const& axis : axes) {
            axis->update();
            deriv[axis] = axis->dt(axis->windowTime);
            integ[axis] = axis->integ(axis->windowTime);
            variance[axis] = axis->variance(axis->windowTime);
        }
        
        current.readAxes();
        
        hdi->insert(space->current.filter(Hidden, false), space->deriv.filter(Vec::Invariant), dT);
        
        if(controller != nullptr) {
            controller->executeDirectives();
        }
        
        target.filter(Vec::Variant).writeAxes();
        
        /\*
        std::cout << "Current: " << current << " -> " << target << "\n";
        Vec delta = (target - current).filter(Vec::Invariant).filter(target);
        std::cout << "\tDelta:\n";
        delta.debugPrint("\t\t");
        
        Path resultPath;
        resultPath.startPt() = this->current;//.filter(Vec::Variant);
        resultPath.next(this->target | this->current);
        Vec variants = this->calculateVariants(delta, resultPath);//.filter(Vec::Variant);
        std::cout << "\tVariants:\n";
        variants.debugPrint("\t\t");
        variants.writeAxes();
        *\/
        return true;
    }
*/
    void Space::read(const Num& dT) {
        _unused(dT);
        deltaTime = dT;
        for(Axis *const& axis : axes) {
            axis->update();
            deriv[axis] = axis->dt(axis->windowTime);
            integ[axis] = axis->integ(axis->windowTime);
            variance[axis] = axis->variance(axis->windowTime);
        }
        
        current.readAxes();
    }
    void Space::write(const Num& dT) {
        _unused(dT);
        //std::cout << "WRITE " << target.filter(Vec::Variant) << "\n";
        target.filter(Vec::Variant).writeAxes();
        for(Axis *const& axis : axes) {
            if(!axis->variant()) continue;
            axis->update();
            //std::cout << "\t" << axis->name << " = " << axis->get() << "\n";
        }
        
    }
        
    Vec Space::point() const {
        Vec ret = Vec();
        for(auto& axis : axes) ret[axis] = 0.0;
        return ret;
    }
    
    void Space::setTarget(Vec pt) {
        target = pt;
    }
    
    void Space::debugPrint() {
        std::cout << "Begin spatial debug seq, dim=" << axes.size() << ":\n";
        for(auto& axis : axes) {
            std::cout << "\t" << axis->typeString() << " " << axis->name << " = " << axis->get() << ", d/dt = " << axis->dt(1.0) << ", d^-1/dt = " << axis->integ(1.0) << "\n";
            std::cout << "\t\tmean=" << axis->mean(1.0) << ", variance=" << axis->variance(1.0) << "\n";
        }
    }
    
    Vec Space::calculateVariants(Vec delta, Path& resultPath) {
        Vec ret;
        Paths paths;
        std::cout << "Calculating next for path " << resultPath << "\n";
        std::cout << "\tdesired delta = " << delta.filter(Vec::Invariant) << "\n";
        for(Jacobian& del : controls) {
            Path path = resultPath;
            Num dot = delta.dot(del);
            
            path.meta.dot = dot;
            Num denom = (delta.filter(del).magnitude() * del.magnitude());
            if(denom > 0.0) path.meta.normDot = dot / denom;
            else path.meta.normDot = 0.0;
            
            path.meta.delLength = del.magnitude();
            if(path.meta.normDot < 0.01) continue;//If we don't influence the variable, skip the control point
            std::cout << "\tusing jacobian with del=" << del << "\n";
            std::cout << "\t\tcalculated normDot=" << path.meta.normDot << ", delLen=" << path.meta.delLength << "\n";
            
            path.error += del.distanceTo(delta);
            //std::cout << "\t\tdistance between " << del << " and " << delta << " = " << path.error << "\n";
            //path.length = del.absolute.filter(Vec::Invariant).distanceTo(this->current.filter(Vec::Invariant));
            path.next(del.absolute | current);
            //std::cout << "\tpath from " << path.startPt() << " to " << path.endPt() << " has err=" << path.error << ", length=" << path.length << "\n";
            paths.push(path);
        }
        
        Num lastLength = 1024.0*1024.0*1024.0;
        std::list<Path> pathQ;
        while(!paths.empty()/* && paths.top().length < lastLength - 0.05*/) {
            Path path = paths.top(); paths.pop();
            Vec nextDelta = (path.endPt() - current).filter(Vec::Invariant).filter(path.endPt());
            //if(nextDelta == delta) continue;//Don't allow recursion
            if(nextDelta == path.endPt().filter(Vec::Invariant)) continue;
            
            //std::cout << "\tresolving next delta = " << nextDelta << " at " << path.endPt().filter(Vec::Variant) << "\n";
            /*std::cout << "\tnextDelta magnitude=" << nextDelta.magnitude() << "\n";
            if(nextDelta.magnitude() < 0.1) {
                pathQ.push_back(path);//Found a satisfactory path, return it
                //break;
                continue;
            }*/
            //if(this->calculateVariants(nextDelta, path).magnitude() > 0.0001) {
            Vec prevPt = path.endPt(); Num prevLen = path.length;
            Vec vars = this->calculateVariants(nextDelta, path);
            Num lf = (path.length - prevLen) / path.length;
            
            vars = prevPt * (1.0-lf) + vars * lf;//Interpolate the pre and post requisite paths
            path.endPt() = vars;
            pathQ.push_back(path);
            //}
            continue;
            //Calculate set of deltas to get us to our desired deltas, then add it to our path
            usleep(1000*500);
            //Vec pt = 
            
            const Num length = path.length;
            if(length < lastLength) {
                paths.push(path);
                lastLength = length;
            } else {
                //Dont add it back
            }
        }
        
        /*
        if(paths.empty()) return this->point().filter(Vec::Variant);
        Path top = paths.top();
        ret = top.endPt().filter(Vec::Variant);
        
        Vec tmpPt = resultPath.endPt();
        Vec filterPt = tmpPt.filter(Vec::Variant);
        std::cout << "Found if " << ret << " == " << filterPt << " ? " << (ret == filterPt) << "\n";
        if(ret == filterPt) return this->point().filter(Vec::Variant);
        */
        
        Num totalMults = 0.0, totalDot = 0.0;//, totalLen = 0.0;
        while(!pathQ.empty()) {
            Path path = pathQ.back();
            pathQ.pop_back();
            
            Num weight = path.meta.normDot;
            if(weight <= 0.0) continue;
            std::cout << "\tpath has delLen=" << path.meta.delLength << "\n";
            Num scale = delta.magnitude() / (path.meta.delLength == 0? 1.0 : path.meta.delLength);
            scale = (std::min(1.0, scale));
            
            Num mult = weight * scale;
            std::cout << "\t\tmerging path with weight=" << mult << " from d=" << weight << ", s=" << scale << ", vec=" << path.endPt().filter(Vec::Variant) << "\n";
            
            ret = ret + path.endPt().filter(Vec::Variant) * mult;
            totalMults += mult;
            totalDot += path.meta.normDot;
        }
        
        if(totalDot > 0.0) ret = ret / totalDot;//totalMults;
        
        if(totalDot > 0.0 && totalMults > 0.0) {
            resultPath.next(ret);
            //resultPath.meta.delLength += ret.magnitude();
        }
        
        return ret;
    }
};