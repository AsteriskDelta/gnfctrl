#include "MDNode.impl.h"
#include "Jacobian.h"

namespace GNFC {
template class MDNode<Vec, RecordedJacobian>;
};