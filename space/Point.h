#ifndef POINT_H
#define POINT_H
#include "GNFInclude.h"
#include <unordered_map>
#include <list>

#define EQOP(OP) template<typename T>\
inline auto& operator CONCAT(OP,=)(const T& o) {\
    (*this) = (*this) OP o;\
    return *this;\
}

#define ALL_EQ_OPS \
EQOP(+);\
EQOP(-);\
EQOP(*);\
EQOP(/);\
EQOP(|);\
EQOP(&);

namespace GNFC {

    class Vec : public Common {
    public:
        typedef Num Value;
        typedef Axis AxisType;
        
        Vec();
        Vec(const Value& def);
        Vec(const Vec& orig);
        virtual ~Vec();
        
        Vec& operator=(const Vec& o);
        
        enum DataType {
            Invalid = 0,
            Variant,
            Invariant,
            Hidden
        };
        
        struct AxisData {
            Axis *axis;
            Num value;
            
            bool matches(DataType type) const;
        };
        
        Num& get(Axis* axis, bool ins = true);
        inline Num& operator[](Axis *const& axis) {
            return this->get(axis);
        }
        inline Num& operator[](Axis *const& axis) const {
            return const_cast<Vec*>(this)->get(axis, false);
        }
        
        bool remove(Axis *axis);
        
        inline auto begin() {
            return data.begin();
        }
        inline auto end() {
            return data.end();
        }
        inline auto begin() const {
            return data.begin();
        }
        inline auto end() const {
            return data.end();
        }
        
        void readAxes();
        void writeAxes();
        
        bool has(Axis *axis) const;
        
        Num magnitude() const;
        Num magnitude2() const;
        
        Num distanceTo(const Vec &o) const;
        Num distanceTo2(const Vec &o) const;
        
        Num relDistanceTo(const Vec &o) const;
        Num relDistanceTo2(const Vec &o) const;
        
        Num dot(const Vec& ot) const;
        
        Vec& makeRelative(bool raw = false);
        Vec& clamp();
        
        Vec nonZero() const;
        
        inline explicit operator bool() const {
            return this->magnitude() > 0.001;
        }
        
        inline void touch(Axis *const& axis) {
            (*this)[axis] = Num();
        }
        
        Vec filter(DataType type, bool keep = true) const;
        Vec filter(const Vec& o, bool keep = true) const;
        Vec filter(AxisType *const& axis, bool keep = true) const;
        Vec operator-(const Vec& o) const;
        Vec operator+(const Vec& o) const;
        
        Vec operator*(const Vec& o) const;
        Vec operator/(const Vec& o) const;
        Vec operator*(const Num& o) const;
        Vec operator/(const Num& o) const;
        
        bool operator==(const Vec& o) const;
        inline bool operator!=(const Vec& o) const {
            return !(*this == o);
        }
        bool operator<(const Vec& o) const;
        bool operator >=(const Vec& o) const;
        bool operator <=(const Vec& o) const;
        
        bool operator>(const Num& o) const;
        bool operator>=(const Num& o) const;
        
        Vec operator|(const Vec& o) const;
        Vec operator&(const Vec& o) const;
        
        ALL_EQ_OPS;
        
        void clear();
        void debugPrint(const std::string &prefix = "") const;
        std::string str() const;
        std::string iden(const Vec& to = Vec()) const;
        
        inline bool empty() const {
            return data.empty();
        }
    private:
        std::list<AxisData> data;
        std::unordered_map<Axis*, decltype(data)::iterator> map;
        
        Num defaultValue;
    };
    
    template<> inline auto max<Vec>(const Vec& a, const Vec& b) {
        Vec ret = a;
        for(const Vec::AxisData& dat : b) {
            if(a.has(dat.axis)) ret[dat.axis] = ::std::max(dat.value, ret[dat.axis]);
            else ret[dat.axis] = dat.value;
        }
        return ret;
    }
    template<> inline auto min<Vec>(const Vec& a, const Vec& b) {
        Vec ret = a;
        for(const Vec::AxisData& dat : b) {
            if(a.has(dat.axis)) ret[dat.axis] = ::std::min(dat.value, ret[dat.axis]);
            else ret[dat.axis] = dat.value;
        }
        return ret;
    }
};
/*
    template <> 
    ::GNFC::Vec std::max<::GNFC::Vec>(const ::GNFC::Vec& a, const ::GNFC::Vec& b) {
        ::GNFC::Vec ret;
        for(const ::GNFC::Vec::AxisData& dat : a) {
            ret[dat.axis] = std::max(dat.value, b[dat.axis]);
        }
        return ret | b;
    }
    template <> 
    ::GNFC::Vec std::min<::GNFC::Vec>(const ::GNFC::Vec& a, const ::GNFC::Vec& b) {
        ::GNFC::Vec ret;
        for(const ::GNFC::Vec::AxisData& dat : a) {
            ret[dat.axis] = std::min(dat.value, b[dat.axis]);
        }
        return ret | b;
    }*/
#endif /* POINT_H */

