#ifndef PATH_H
#define PATH_H
#include "GNFInclude.h"
#include "Point.h"
#include <vector>
#include <queue>

namespace GNFC {

    class Path : public Common {
    public:
        Path();
        Path(const Vec& start);
        Path(const Path& orig);
        virtual ~Path();
        
        Num length, error, eta;
        struct Node : Vec {
            inline Node() : Vec() {};
            inline Node(const Vec& p) : Vec(p) {};
        };
        std::vector<Node> nodes;
        struct {
            Num dot, normDot, delLength;
            bool isComplete = false;
        } meta;
        
        Vec evaluate(const Vec& current);
        
        void next(Vec pt);
        
        inline Num err() const {
            return error;
        }
        
        inline explicit operator bool() const {
            return length > 0.0;
        }
        
        inline Vec& operator[](unsigned int i) {
            return nodes[i];
        }
        inline Vec& startPt() {
            if(nodes.size()  < 1) nodes.push_back(Node());
            return nodes[0];
        }
        inline Vec& endPt() {
            if(nodes.size() < 1) nodes.push_back(Node());
            return *nodes.rbegin();
        }
        
        inline Num composite() const {
            return eta;//length * error;//error;//length + error;
        }
        inline bool operator <(const Path& o) const {
            return this->composite() < o.composite();
        }
        
        std::string str() const;
        
        inline bool complete() const {
            return meta.isComplete;
        }
        
        void scoreTo(const Vec& ideal);
        
        void clear();
        
        Path& operator|=(const Vec& vec);
    private:

    };

    
    typedef std::priority_queue<Path> Paths;
};

#endif /* PATH_H */

