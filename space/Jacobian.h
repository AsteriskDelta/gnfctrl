#ifndef JACOBIAN_H
#define JACOBIAN_H
#include "GNFInclude.h"
#include "Point.h"
#include "History.h"
#include "Axis.h"
#include <unordered_map>

namespace GNFC {
    class Axis;

    class Jacobian  : public Vec {
    public:
        Jacobian();
        Jacobian(const Jacobian& orig);
        virtual ~Jacobian();
        
        Vec absolute;
        
        void clear();
    private:

    };
    
    struct RecordedJacobian : Jacobian {
        
        RecordedJacobian() : Jacobian() {
            absoluteHistory.setDuration(600.0);
            history.setDuration(60.0);
        }
        
        //std::unordered_map<Axis*, History<Num,Num>> historyMap;
        History<Vec, Num> history;
        /*inline History<Num,Num>& history(Axis *reqAxis) {
            return historyMap[reqAxis];
        }*/
        
        History<Vec, Num> absoluteHistory;
        
        /*inline void update() {
            for(auto it = this->begin(); it != this->end(); ++it) {
                const AxisData& dat = *it;//Jacobian, so this is d/dt
                historyMap[dat.axis].add(dat.value);
            }
        }*/
        /*inline void update(Axis *axis) {
            (*this)[axis] = this->history(axis).mean(16.0);
            //(*this)[axis] = this->history(axis).dt(axis->windowTime);
        }*/
        inline void update() {
            (*static_cast<Vec*>(this)) = history.mean(300.0);
        }
        
        inline void addContribution(const Vec& vec, const Num& dT) {
            absoluteHistory.add(vec, dT);
            //std::cout << "\nadd abs variance contribution of " << (vec) << "\n\thas mean=" << absoluteHistory.mean(60) << ", var=" << absoluteHistory.variance(60) << "\n";
        }
        
        inline bool mature() const {
            return history.samples() > 2;//historyMap.size() > 0 && historyMap.begin()->second.samples() > 6;
        }
    };
}

#endif /* JACOBIAN_H */

