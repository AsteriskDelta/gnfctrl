#include "Path.h"
#include <sstream>

namespace GNFC {

    Path::Path() : length(0), error(std::numeric_limits<Num>::max()), eta(std::numeric_limits<Num>::max()), nodes(), meta() {
    }

    Path::Path(const Vec& start) : length(0), error(std::numeric_limits<Num>::max()), eta(std::numeric_limits<Num>::max()), nodes(), meta() {
        //nodes.push_back(Node(start));
        this->next(start);
    }
    
    Path::Path(const Path& orig) : length(orig.length), error(orig.error), eta(orig.eta), nodes(orig.nodes), meta(orig.meta) {
    }

    Path::~Path() {
    }
    
    void Path::next(Vec pt) {
        //std::cout << "path add " << pt << "\n";
        //this->length += this->endPt().filter(Vec::Invariant).distanceTo(pt.filter(Vec::Invariant));
        nodes.push_back(Node(pt.filter(Vec::Hidden, false)));
    }
    
    Vec Path::evaluate(const Vec& current) {
        std::cout << "Path eval of sz=" << nodes.size() << "\n";
        
        if(nodes.empty()) return Vec();
        else if(nodes.size() == 1) return nodes.front();
        //Find the nodes which our current point is between...ish
        Node *a = nullptr, *b = nullptr;
        Num ratio = 1.0, bestError = std::numeric_limits<Num>::max();
        for(auto it = std::next(nodes.begin()); it != nodes.end(); ++it) {
            auto prev = std::prev(it);
            
            Vec mask = *prev | *it;
            
            //Triangle inequality ftw
            Num idealDist = prev->distanceTo(*it);
            Num actualDist = prev->distanceTo(current & mask) + it->distanceTo(current & mask);
            Num err = actualDist / idealDist - 1.0;
            
            if(err < bestError) {
                a = &(*prev);
                b = &(*it);
                ratio = prev->distanceTo(current & mask) / actualDist;
                bestError = err;
            }
        }
        
        if(a == nullptr || b == nullptr) {
            std::cerr << "PATHING ERR, NO NODES\n";
            return Vec();
        }
        
        std::cout << "\tpathing via ratio=" << ratio << "\n";
        
        return ((*a) * (1.0 - ratio) + (*b) * (ratio)).filter(Vec::Invariant) | b->filter(Vec::Variant);
    }
    
    std::string Path::str() const {
        std::stringstream ss;
        ss << "Path[" << nodes.size() << "](";
        for(auto it = nodes.begin(); it != nodes.end(); ++it) {
            if(it != nodes.begin()) ss << ", ";
            ss << *it;
        }
        ss << ")";
        return ss.str();
    }

    void Path::clear() {
        nodes.clear();
        length = 0;
        error = 0;
        meta = decltype(meta)();
    }
    
    void Path::scoreTo(const Vec& ideal) {
        /*if(nodes.empty()) {
            error = length = 0;
            return;
        }*/
        error = this->endPt().distanceTo(ideal);
        length = 0;
        for(auto it = std::next(nodes.begin()); it != nodes.end(); ++it) {
            auto prev = std::prev(it);
            length += prev->distanceTo(*it);
        }
    }
    
    Path& Path::operator|=(const Vec& vec) {
        for(Node& node : nodes) node |= vec;
        if(nodes.empty()) this->next(vec);
        return *this;
    }
};