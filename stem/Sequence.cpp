#include "Sequence.h"
#include "Space.h"

namespace GNFC {
    void SequenceInstance::setup(Space *sp, Sequence *par) {
        space = sp;
        parent = par;
        step = 0;
    }

    bool SequenceInstance::complete() const {
        return parent == nullptr || step >= parent->totalSteps();
    }

    Vec SequenceInstance::evaluate() {
        if(space == nullptr || parent == nullptr) return Vec();
        
        return parent->evaluate(this);
    }

    Sequence::Sequence() {
    }

    Sequence::~Sequence() {
    }

    Vec Sequence::evaluate(SequenceInstance *instance) {
        std::cout << "BEGIN SEQUENCE EVAL\n";
        unsigned int idx = instance->step;
        if(idx >= steps().size()) return Vec();
        
        Step *step = &(steps()[idx]);
        if(instance->startTime == 0.0) instance->startTime = CurrentTime();
        
        std::cout << "\tSEQ using current=" << instance->space->current  << ", step=" << *step << "\n";
        const Num err = step->distanceTo(instance->space->current & *step);
        std::cout << "\tSEQUENCE ERR = " << err << " <? " << ((instance->space->variance & *step).magnitude()*instance->space->deltaTime) << "\n";
        if(instance->convergeTime == 0.0 && (err <= (instance->space->deriv & *step).magnitude()*instance->space->deltaTime || step->instant)) {
            instance->convergeTime = CurrentTime();
            std::cout << "SEQUENCE CONVERGED, hold=" << step->holdTime << "\n";
            //Pause();
        }
        
        if(instance->convergeTime != 0.0 && instance->convergeTime + step->holdTime <= CurrentTime()) {
            instance->startTime = instance->convergeTime = 0.0;
            instance->step = instance->step + 1;
            std::cout << "Sequence advanced to step #" << instance->step << "\n";
            //Pause();
            return this->evaluate(instance);
        }
        
        return *step;
    }
        
    Sequence::Step* Sequence::add(const Step& step) {
        steps().push_back(step);
        return &(steps().back());
    }
    Sequence::Step* Sequence::add(const Vec& vec) {
        Step tmpStep = Step();
        (*static_cast<Vec*>(&tmpStep)) = vec;
        return this->add(tmpStep);
    }
    /*
    Sequence& Sequence::operator|=(const Vec& vec) {
        for(auto& step : steps) steps |= vec;
    }*/
};