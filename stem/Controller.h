#ifndef CONTROLLER_H
#define CONTROLLER_H
#include "GNFInclude.h"
#include "Point.h"
#include <list>
#include "Planner.h"

namespace GNFC {

    struct ControllerRoot {
        struct {
            Space *space;
            MDTree *tree;
        } alloc;
        ControllerRoot();
    };
    
    class Controller : public ControllerRoot, public Planner {
    public:
        Controller();
        virtual ~Controller();
        
        std::list<Directive*> directives;
        std::list<Axis*> intrinsics;
        
        void executeDirectives();
        
        void update(const Num& dT);
        
        void addDirective(Directive *dPtr);
        void addIntrinsic(Axis *axis);
        void addAxis(Axis *axis);
        
        UserControl* user();
        
        const Vec& current() const;
        const Vec& target() const;
        
        void debugPrint();
    private:
        UserControl *userControl;
        PathError *pathErr;
    };
};

#endif /* CONTROLLER_H */

