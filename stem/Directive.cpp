#include "Directive.h"
#include "Space.h"
#include "Controller.h"

namespace GNFC {
    
    void Directive::RuleSet::setRange(const Vec& start, const Vec& end) {
        soft.min = start;
        soft.max = end;
        hard.min = end;
        enabled = true;
    }

    Directive::Directive(Controller *ctrl) : current(), target(), deriv(), priority(0.0),
    action(), instance(), flags(), controller(ctrl), activation(0.0) {
        
    }
    
    Space* Directive::space() const {
        return controller->space;
    }

    Directive::~Directive() {
    }

    void Directive::validate() {

    }

    void Directive::changed() {

    }
    
    void Directive::invoked() {
        //std::cout << "TRIGGER DIRECTIVE " << this->name() << " at CUR=" << space()->current << "\n";
        //Pause();
    }
    
    void Directive::completed() {
        
    }

    void Directive::execute() {
        const bool curTriggered = this->triggered();
        if(!curTriggered && !flags.triggered) return;
        else if(!flags.triggered) {
            flags.triggered = true;
            this->invoked();
        }
        
        //Execute the sequence
        if(!instance) instance.setup(space(), &action);
        Vec targ = instance.evaluate();
        evaluated = targ;
        
        //std::cout << "lerping space's target to " << targ << " with activation=" << activation << ", hard="<< flags.hard<<"\n";
        
        if(flags.hard) space()->target = targ;//Override anything
        else {//Calculate the directive's influence
            Vec interpTarget = targ * activation + space()->target.filter(targ) * (1.0 - activation);
            space()->target = space()->target.filter(interpTarget, false) | interpTarget;
        }
        //std::cout << "Space target is " << space()->target << "\n";
        
        bool complete = false;
        complete |= /*flags.mustComplete && */instance.complete();
        complete |= !flags.mustComplete && !curTriggered;
        
        if(complete) {
            flags.triggered = false;
            this->completed();
        }
    }

    bool Directive::triggered() {
        int evals = 0x0;
        activation = 0.0;
        if(current)     evals |= this->evaluateRuleSet(current, space()->current);
        if(target)      evals |= this->evaluateRuleSet(target, space()->target);
        if(deriv)       evals |= this->evaluateRuleSet(deriv, space()->deriv);
        if(variance)    evals |= this->evaluateRuleSet(variance, space()->variance);
        
        if(evals == 0) return false;
        else if(evals == 1) {
            flags.hard = false;
            return true;
        } else {
            flags.hard = true;
            return true;
        }
    }
    
    int Directive::evaluateRuleSet(RuleSet& set, const Vec& state) {
        bool tSoft = false, tHard = false;
        Num deltaSoft = 0.0;
        /*
        if(set.soft.min && (state & set.soft.min) < set.soft.min) {
            tSoft = true;
            deltaSoft = (set.soft.min - (state & set.soft.min)).magnitude() / (set.soft.min - set.hard.min.filter(set.soft.min)).magnitude();
        } else if(set.soft.max && set.soft.max < (state & set.soft.max)) {
            tSoft = true;
            deltaSoft = ((state & set.soft.max) - set.soft.max).magnitude() / (set.hard.max.filter(set.soft.max) - set.soft.max).magnitude();
        }
        if(tSoft) activation = std::max(activation, deltaSoft);
        
        if(set.hard.min && (state & set.hard.min) < set.hard.min) tHard = true;
        else if(set.hard.max && set.hard.max < (state & set.hard.max)) tHard = true;
        */
        if((set.soft.min || set.soft.max) && 
                (!set.soft.min || set.soft.min <= (state & set.soft.min)) &&
                (!set.soft.max || (state & set.soft.max) <= set.soft.max)) {
            tSoft = true;
            if(!set.soft.max || set.soft.max == set.soft.min) deltaSoft = 1.0;
            else deltaSoft = set.soft.min.distanceTo(state & set.soft.min) / set.soft.min.distanceTo(set.soft.max);
            
            //std::cout << "\t\tdeltaSoft=" << deltaSoft << " from " << set.soft.min.distanceTo(state & set.soft.min) << " / " << set.soft.min.distanceTo(set.soft.max)<< "\n";
            activation = std::max(activation, deltaSoft);
        }
        
        if((set.hard.min || set.hard.max) && 
                (!set.hard.min || set.hard.min <= (state & set.hard.min)) &&
                (!set.hard.max || (state & set.hard.max) <= set.hard.max)) {
            tHard = true;
        }
        
        if(tSoft || tHard) {
            //std::cout << "\tRULE SET EVAL TRUE: " << set.soft.min << " on cur=" << state << ", activation = " << activation << "\n";
            //Pause();
        }
        
        if(tHard) {
            activation = 1.0;
            return 2;
        }
        else if(tSoft) return 1;
        else return 0;
    }
};
