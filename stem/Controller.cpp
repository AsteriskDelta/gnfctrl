#include "Controller.h"
#include "Directive.h"
#include "Space.h"
#include <algorithm>
#include "PathError.h"
#include "UserControl.h"
#include "Explore.h"
#include "PathError.h"

namespace GNFC {

    ControllerRoot::ControllerRoot() {
        alloc.space = new Space();
        alloc.tree = new MDTree();
        alloc.space->hdi = alloc.tree;
    }

    Controller::Controller() : ControllerRoot(), Planner(ControllerRoot::alloc.space, ControllerRoot::alloc.tree) {
        
        pathErr = new PathError(this);
        this->addIntrinsic(pathErr);
        this->addDirective(new Explore(this, pathErr));
        userControl = new UserControl(this);
        this->addDirective(userControl);
    }

    Controller::~Controller() {
        for(Directive *directive : directives) delete directive;
    }
    
    UserControl* Controller::user() {
        return userControl;
    }

    void Controller::executeDirectives() {
        space->target.clear();
        for(auto it = directives.begin(); it != directives.end(); ++it) {
            Directive *directive = *it;
            //directive->controller = this;
            std::cout << "\nEXEC DIRECTIVE " << directive->name() << "\n";
            directive->execute();
            std::cout << "END DIRECTIVE " << directive->name() << "\n\n";
        }
    }
    
    void Controller::addDirective(Directive *dPtr) {
        directives.push_back(dPtr);
        directives.sort([](Directive *const& a, Directive *const& b ) { return (*a < *b); });
    }
    
    void Controller::addIntrinsic(Axis *axis) {
        intrinsics.push_back(axis);
        space->add(axis);
    }
    void Controller::addAxis(Axis *axis) {
        space->add(axis);
    }
    
    void Controller::update(const Num& dT) {
        space->controller = this;
        //space->update(dT);
        space->read(dT);
        //std::cout << "HDI INSERT\n";
        this->hdi->insert(space->current.filter(Vec::Hidden, false), space->deriv.filter(Vec::Hidden, false), dT);
        
        //this->executeDirectives();
        //std::cout << "space has target " << space->target << "\n";
        //this->setTarget(space->target);
        
        //Update hidden variable to reflect controller state
        //space->current[pathError] = this->path()->error();
        //Vec pathEval = path().evaluate(space->current);
        //std::cout << "path evaluated to " << pathEval << " from " << path() << "\n";
        
        Vec pathEval;
        for(auto it = directives.begin(); it != directives.end(); ++it) {
            Directive *directive = *it;
            std::cout << "\nEXEC DIRECTIVE " << directive->name() << "\n";
            directive->execute();
            this->setTarget(directive->evaluated);
            
            Vec direct = path().evaluate(space->current);
            direct[pathErr] = this->path().err();//->error();
            pathEval = direct * directive->activation + pathEval * (1.0 - directive->activation);
            space->target = pathEval;//Set so that future directives can act based on target
            std::cout << "END DIRECTIVE " << directive->name() << "\n\n";
        }
        
        space->setTarget(pathEval);
        space->write(dT);
    }
    
    const Vec& Controller::current() const {
        return space->current;
    }
    const Vec& Controller::target() const {
        return space->target;
    }
    
    void Controller::debugPrint() {
        std::cout << "Begin Controller debug: " << this->current().filter(target()) << " -> " << this->target() << "\n";
        for(Directive* directive : directives) {
            std::cout << "\t";
            if(directive->flags.triggered) std::cout << "[" << round(directive->activation * 10000.0)/100.0 << "%]";
            else std::cout << "[INAC]";
            
            std::cout << "\t" << directive->name() << ", priority=" << directive->priority << "\n";
            std::cout << "\t\t\ttarget = " << (directive->instance? directive->instance.evaluate() : Vec()) << "\n";
        }
    }
};