#ifndef DIRECTIVE_H
#define DIRECTIVE_H
#include "GNFInclude.h"
#include "Point.h"
#include "Sequence.h"

namespace GNFC {

    class Directive {
        friend class Controller;
    public:
        Directive(Controller *ctrl = nullptr);
        virtual ~Directive();
        
        struct Rule {
            Vec min, max;
        };
        struct RuleSet {
            Rule soft, hard;
            inline void min(const Vec& v) {
                //soft.min = hard.min = v;
                soft.min = v;
                enabled = true;
            }
            inline void max(const Vec& v) {
                //soft.max = hard.max = v;
                soft.max = v;
                enabled = true;
            }
            void setRange(const Vec& start, const Vec& end);
            bool enabled = false;
            inline explicit operator bool() const {
                return enabled;
            }
        };
        
        RuleSet current, target, deriv, variance;
        Vec evaluated;
        
        virtual void validate();
        
        virtual void changed();
        virtual void execute();
        virtual void invoked();//Called prior to first of many execute() calls- set up directive's state if needed
        virtual void completed();//Called after trigger conditions have been fulfilled AND THEN expired
        
        virtual bool triggered();
        
        Num priority;
        Sequence action;
        SequenceInstance instance;
        struct {
            bool mustComplete = false;
            bool triggered = false;
            bool hard = false;
        } flags;
        
        Controller *controller;
        Space* space() const;
        
        inline bool operator<(const Directive& o) const {
            return priority < o.priority;
        }
        
        std::string& name() {
            return meta.name;
        }
        Num activation;
    protected:
        struct {
            std::string name = "";
        } meta;
        
        int evaluateRuleSet(RuleSet& set, const Vec& state);
    };
};

#endif /* DIRECTIVE_H */

