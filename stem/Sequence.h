#ifndef SEQUENCE_H
#define SEQUENCE_H
#include "GNFInclude.h"
#include "Point.h"
#include <vector>

namespace GNFC {
    struct SequenceInstance {
        Space *space = nullptr;
        Sequence *parent = nullptr;
        unsigned int step = 0;
        
        Num startTime = 0.0, convergeTime = 0.0;
        
        void setup(Space *sp, Sequence *par);
        
        bool complete() const;
        inline explicit operator bool() const {
            return !this->complete();
        }
        
        Vec evaluate();
    };
    class Sequence {
    public:
        Sequence();
        virtual ~Sequence();
        
        struct Step : public Vec {
            Num holdTime = 0.0;
            bool instant = false;
        };
        
        virtual Vec evaluate(SequenceInstance *instance);
        
        virtual Step* add(const Step& step);
        virtual Step* add(const Vec& step);
        
        inline unsigned int totalSteps() const {
            return stepVec.size();
        }
        inline std::vector<Step>& steps() {
            return stepVec;
        }
        
        inline void clear() {
            stepVec.clear();
        }
        
        //Sequence& operator|=(const Vec& vec);
    private:
        std::vector<Step> stepVec;
    };
};

#endif /* SEQUENCE_H */

