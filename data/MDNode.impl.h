#include "MDNode.h"
#include <array>
#include "Axis.h"
#include <limits>
#include <sstream>
#include <fstream>
#include "Point.h"

#define MDN_TPL template<typename VEC, typename VAL>
#define MDNT MDNode<VEC,VAL>

namespace GNFC {

    MDN_TPL
    MDNT::MDNode() : parent(nullptr), _limiter(0), differentiated(false), indivisible(false) {
        for (auto& ptr : splits) ptr = nullptr;
        static unsigned int llIDGlobal = 0;
        llID = llIDGlobal++;
        children.reserve(64);
    }

    //MDN_TPL
    //MDNT::MDNode(const MDNode& orig) {
    //}

    MDN_TPL
    MDNT::~MDNode() {
        for (Self* child : children) {
            delete child;
        }
    }

    MDN_TPL
    typename MDNT::Self* MDNT::insert(const typename MDNT::Vector& state, const Vector& record, const typename MDNT::VectorValue& weight) {
        Set set = this->resolve(state);
        Self *ret = nullptr;
        //std::cout << "hdi insert for " << state << " got " << set.size() << " nodes\n";
        for (Weighted& link : set) {
            //Record the state's occurance
            std::lock_guard<std::mutex> lck(link.node->modMtx);
            link.node->value.addContribution(state, weight);
            //if (link.hasInfluence) {
            //std::cout << "\tassimilate to " << link.node->origin << " + " << link.node->span << "\n";
            link.node->assimilate(state, record, link.weight * weight, link.hasInfluence);
            ret = link.node;
            //}
        }
        return ret;
    }

    MDN_TPL
    typename MDNT::Self* MDNT::assimilate(const typename MDNT::Vector& state, const Vector& record, const typename MDNT::VectorValue& weight, const bool& leaf) {
        //std::lock_guard<std::mutex> lck(modMtx);
        bool doSplit = false;

        struct SplitCanidate {
            Axis *obj = nullptr;
            Num variance = -1.0;
        };

        value.history.add(record, weight);
        value.update();

        if (leaf && !this->indivisible) {
            Vec nodeVariance = value.history.variance(300.0); //.makeRelative(true);
            Vec nodeRange = value.history.range(300.0); //.makeRelative(true);

            for (auto it = record.begin(); it != record.end(); ++it) {
                Num normVariance = nodeVariance[it->axis]; //value.history.variance(it->axis->windowTime)[];
                Num normRange = nodeRange[it->axis]; //value.history(it->axis).range(it->axis->windowTime / 2.0);
                Num threshVariance = (1.0 - normRange / span[it->axis]) * 0.666;//std::min(0.0 + (normRange / this->span[it->axis]), 1.0 - (normRange / this->span[it->axis]));

                //Via Popoviciu's inequality
                Num maxVariance = 0.25 * normRange * normRange;
                //std::cout << "val " << record << " into var=" << normVariance << " / " << maxVariance << " = " << (normVariance / maxVariance ) << "\n";
                //std::cout << "\tfrom range=" << normRange << " , threshold="<<threshVariance<<"\n";//, could="<<couldSplit<<"\n";

                if ((maxVariance > 0.01 && normVariance / maxVariance > threshVariance)) {
                    doSplit = true;
                    //std::cout << "\tsplitting" << it->axis->name << ", var = " << normVariance << " > " << maxVariance << " from range " <<
                    //        value.history(it->axis).range(smpDuration * 2.0) << " with mean " << value.history(it->axis).mean(smpDuration * 2.0) << "\n";
                    //Pause();
                }
            }

            if (doSplit && value.mature()) {
                //std::cout << "splitting due to variance " << minVariance << "\n";
                if (_limiter > 1024) {
                    std::cerr << "LIMIT REACHED, NOT SPLITTING\n";
                    throw std::exception();
                }

                if (!this->differentiated) {//Assign our split axes
                    std::array<SplitCanidate, SplitRate> splitAxes;
                    Num minVariance = -1.0;
                    Vec absVariance = value.absoluteHistory.variance(300.0); //.makeRelative(true);
                    Vec absRange = value.absoluteHistory.range(300.0);

                    //Take the axes with the highest absolute variance (ie, split the most samples with each sector)
                    Vec curSpan = span.filter(state);
                    bool hasSplits = false;
                    for (auto it = curSpan.begin(); it != curSpan.end(); ++it) {
                        Num normRange = absRange[it->axis]; //value.history(it->axis).range(it->axis->windowTime / 2.0);

                        //Via Popoviciu's inequality
                        Num maxVariance = 0.25 * normRange * normRange;
                        Num normVariance = (maxVariance == 0.0) ? 0.0 : absVariance[it->axis] / maxVariance;

                        //Keep values with miniscule ranges from being subdivided
                        bool couldSplit = it->axis->canSubdivide(this->span[it->axis]); // && maxVariance > 0.01;
                        hasSplits |= couldSplit;

                        if (couldSplit && normVariance >= minVariance) {
                            Num newMinVariance = std::numeric_limits<Num>::max();
                            //std::cout << "\tconsidering " << it->axis->name << " to split, var = " << normVariance << "..\n";
                            bool written = false;
                            for (unsigned int i = 0; i < SplitRate; i++) {
                                if (!written && splitAxes[i].variance == minVariance) {
                                    //std::cout << "\t\twriting axis " << it->axis->name << " to idx=" << i << "\n";
                                    splitAxes[i] = SplitCanidate{it->axis, normVariance};
                                    written = true;
                                }
                                newMinVariance = std::min(newMinVariance, splitAxes[i].variance);
                            }
                            minVariance = newMinVariance;
                        }
                    }

                    if (!hasSplits) {//Can't subdivide, just adjust quantity and hope for the best
                        this->indivisible = true;
                        return this;
                    }

                    //std::cout << "SPLIT L=" << this->_limiter << " from " << state << " on axes:\n";
                    for (unsigned int i = 0; i < SplitRate; i++) {
                        this->splits[i] = splitAxes[i].obj;
                        //if (splitAxes[i].obj != nullptr) std::cout << "\t" << splitAxes[i].obj->name << "\n";
                    }
                    this->differentiated = true;

                    //std::cout << "differentiated node at " << this->origin.filter(Vec::Variant) << " -> " << (origin + span).filter(Vec::Variant) << "\n";
                    //Pause();
                }
                //std::cout << "this origin=" << this->origin << ", span=" << this->span << "\n";

                Self *child = new Self();
                child->parent = this;
                child->_limiter = this->_limiter + 1;

                //Determine the span and origin of the child to contain 'state'
                child->span = this->span;
                child->origin = this->origin;

                //child->span = child->span * 2.0;
                //std::cout << "par: " << span << "\nchild:" << child->span << "\n";

                for (Axis * const& axis : splits) {
                    if (axis == nullptr) continue;

                    child->span[axis] = child->span[axis] / 2.0; //Divide this dimension in half
                    if (state[axis] >= this->origin[axis] + child->span[axis]) {
                        child->origin[axis] = child->origin[axis] + child->span[axis];
                    } else {
                        //Just keep our origin for the dimension in question
                    }
                }
                child->value.absolute = child->origin + child->span / 2.0;
                //std::cout << "this origin=" << this->origin << ", span=" << this->span << ", childrenCnt = " << this->children.size() << "\n";
                //std::cout << "Added child subset " << child->origin.filter(Vec::Variant) << " -> " << (child->origin + child->span).filter(Vec::Variant) << " at level " << this->_limiter << "\n";
                //std::cout << "\tto contain point " << state << " ? " << child->contains(state) << "\n";
                //Pause();
                //sleep(3);
                children.push_back(child);
                //child->assimilate(child->center(), this->value, weight * this->value.absoluteHistory.samples() / 2.0);
                //child->assimilate(state, record, weight * 4.0);
                child->assimilate(state, record, weight * 1.0, true);
                return child;
            } else {
                //We already added to the value history before, so nothing to do here
            }

        }

        return this;
    }

    MDN_TPL
    //template<typename FN_T>
    typename MDNT::Vector MDNT::query(const typename MDNT::Vector& state, unsigned int *rDepth, Num *rDist/*, const FN_T* resolver*/) {
        Set set = this->resolve(state);
        Vector ret;

        //std::cout << "\treturning query for set size=" << set.size() << "\n";
        Num totalWeight = 0.0;
        Num dists = 0.0;
        for (Weighted& nodeInfo : set) {
            //std::cout << "\t\t" << nodeInfo.node->value<<" * "<<nodeInfo.weight<<"\n";
            Num dist = state.relDistanceTo(nodeInfo.node->center());
            Num weight = nodeInfo.weight;// * (1.0 / (1.0 + dist));

            ret = ret + nodeInfo.node->value * weight;
            dists += dist * weight;
            totalWeight += weight;

            if (rDepth != nullptr) (*rDepth) = std::max(*rDepth, nodeInfo.depth);
        }
        if (rDist != nullptr) (*rDist) = dists / totalWeight;
        return ret / totalWeight;
    }

    MDN_TPL
    //template<typename FN_T>
    typename MDNT::Vector MDNT::sample(const typename MDNT::Vector& state, typename MDNT::Vector axes) {
        if (!axes) axes = state;

        Self *baseNode = this->resolveMDN(state);
        Vector dBase = (state - baseNode->center()) & axes;
        Vector dExtreme;

        Num normDists = 0.0, normMags = 0.0, normWeights = 0.0;
        Vector ret;

        for (auto it = dBase.begin(); it != dBase.end(); ++it) {
            const auto& dat = *it;
            //const auto mult = 1.0; //(dBase[dat.axis] >= 0.0) ? 1.0 : 0.0;
            dBase[dat.axis] = (baseNode->span[dat.axis] / 2.0 - fabs(dat.value)); // / 2.0 * mult * 1.1;
            dExtreme[dat.axis] = (baseNode->span[dat.axis] - dBase[dat.axis]);
        }

        dBase *= 1.01;
        dExtreme *= 1.01;

        auto doTap = [&](const Vector & tapPt) -> void {
            Num dist;
            unsigned int depth;
            Vec tap = this->query(tapPt, &depth, &dist);
            Num mag = tap.magnitude();

            dist += state.relDistanceTo(tapPt);

            /*if(dist == 0.0) {
                ret = tap;
                normDists = 1.0;
                break;
            }*/
            dist = std::max(0.001, dist);

            Num weight = 1.0 / (dist);
            normDists += dist;
            normMags += mag;
            normWeights += weight;

            ret += tap * weight;
        };

        auto sampleTap = [&](const Vector & st) -> void {
            if (st.empty()) return;
            for (auto it = st.begin(); it != st.end(); ++it) {
                Vec tapPt = state + st.filter(it->axis); //(it == st.end())? Vec() : st.filter(it->axis);
                //std::cout << "tapping " << tapPt << " for " << st << "\n";
                doTap(tapPt);
                //if(it == st.end()) break;
                /*Self *tap = this->resolveMDN(baseNode->center() + st.filter(it->axis));

                Num normDist = (tap->center() - state).makeRelative(true).magnitude();
                if(normDist < 0.01) normDist = 100.0;
                else normDist = 1.0 / normDist;
                
                normDists += normDist;
                ret += tap->value * normDist;*/

                //sampleTap(st.filter(it->axis, false));
            }
        };
        //while(!dBase.empty()) {
        sampleTap(dBase);
        sampleTap(dExtreme * -1);
        doTap(baseNode->center());
        //sampleTap(dBase * 0.0);
        //dBase.remove(std::prev(dBase.end())->axis);
        //}

        /*{
            Num normDist = (baseNode->center() - state).makeRelative(true).magnitude();
            if(normDist < 0.01) normDist = 100.0;
            else normDist = 1.0 / normDist;
            normDists += normDist;
            ret += baseNode->value * normDist;
        }*/

        if (normWeights == 0.0) return ret;
        else return ret / normWeights;
    }

    MDN_TPL
    bool MDNT::contains(const typename MDNT::Vector& state) {
        //Vector offset = (state - origin) & state;
        //std::cout << "contain test offset=" << offset << " ? " << (offset < span) << " g0 " << (offset >= 0.0) << "\n";
        //return offset < span && offset >= Num(0.0);
        return this->near() <= state && state <= this->far();
    }
    /*
        MDN_TPL
        bool MDNT::consistant(const typename MDNT::Vector& state) {//Should it be encorporated (doesn't exceed local variance threshold for split)
        }
     *//*
    MDN_TPL
    void MDNT::split(const typename MDNT::Vector& axes) {
        
        
        
        
        for(Self *child : children) {
        }
    }
*/

    MDN_TPL
    typename MDNT::Set MDNT::resolve(const typename MDNT::Vector& state) {
        Set set;
        this->resolve(state, set);

        Num totalWeight = 0.0;
        for (Weighted& w : set) totalWeight += w.weight;
        for (Weighted& w : set) w.weight /= totalWeight;

        return set;
    }

    MDN_TPL
    typename MDNT::Self* MDNT::resolveMDN(const typename MDNT::Vector& state) {
        Set set;
        this->resolve(state, set);

        if (set.size() == 0) return nullptr;
        else return set[0].node;
    }

    MDN_TPL
    typename MDNT::Set& MDNT::resolve(const typename MDNT::Vector& state, typename MDNT::Set& set) {
        bool delegated = false;
        for (Self *child : children) {
            //std::cout << "\tchecking if " << child->near() << " -> " << child->far() << " contains " << state << "? " << child->contains(state) << "\n";
            if (child->contains(state)) {
                //set.push_back(Weighted{1.0, child});
                //return set;
                delegated = true;
                child->resolve(state, set);
            }
        }

        //std::cout << "\tadding set to resolve:" << this->origin << " + " << this->span << " contains " << state << "? " << this->contains(state) << "\n";
        //std::cout << "\t\tvalue = " << this->value << ", sz=" << this->value.historyMap.size() <<"\n";
        //Vec spanWeights = (root()->span.filter(state) / this->span.filter(state)).makeRelative(true);
        //spanWeights *= spanWeights;
        //Num weight = (root()->span.filter(state) / this->span.filter(state)).makeRelative(true).magnitude();//1.0 / span.makeRelative(true).magnitude();
        //weight *= weight;
        Num weight = 1.0 / this->span.filter(state).makeRelative(true).magnitude();

        set.push_back(Weighted{weight, this, _limiter, !delegated});
        return set;
    }

    MDN_TPL
    std::string MDNT::iden() const {
        std::stringstream ss;
        ss << llID; //origin.filter(Vec::Hidden, false).iden() << "_" << (origin + span).filter(Vec::Hidden, false).iden();
        return ss.str();
    }

    MDN_TPL
    void MDNT::debugGraph(std::fstream& stream, bool root) {
        //if(this->_limiter > 1) return;
        if (root) {
            stream << "digraph G {\n";
            stream << "\trankdir=LR;\n\tranksep=3.0;\n\tnodesep=2.0;\n\tratio = \"auto\";\n\toverlap=\"false\";\n";
        }
        stream << "\t" << this->iden() << " [label=\"" << value.filter(Vec::Invariant).iden() << "\"];\n\n";
        for (auto *child : children) {
            Vec diff = (span - child->span).nonZero();
            stream << "\t" << this->iden() << " -> " << child->iden() <<
                    //"[taillabel=\""<<(origin&diff).iden(far()&diff) <<"\", "<<
                    "[headlabel=\"" << (child->origin).filter(Vec::Variant).iden(child->far().filter(Vec::Variant)) << "\"];\n";
        }
        for (auto *child : children) child->debugGraph(stream, false);
        if (root) stream << "}\n";
    }


    //Iterator code

    MDN_TPL
    MDNT::iterator::iterator(MDNT::Self *par, const Vec& cen, const Vec& ax) : parent(par), center(cen), distance2(0) {
        if (ax) axes = ax;
        else axes = par->root()->span;
        this->setCurrent(parent->resolveMDN(center));
        start = current;
        visited.insert(start);
    }

    MDN_TPL
    typename MDNT::Value* MDNT::iterator::operator->() {
        return &current->value;
    }

    MDN_TPL
    typename MDNT::iterator& MDNT::iterator::operator++() {
        if (current == nullptr) return *this;

        if (queued.empty()) this->expand();
        if (queued.empty()) {
            current = nullptr;
            return *this;
        }

        Self *next = (*queued.begin()).node;
        queued.erase(queued.begin());

        this->setCurrent(next);
        return *this;
        /*
        Self *nextNode = nullptr;
        Num bestDist2 = std::numeric_limits<Num>::max();
        auto next = std::next(edges.begin());
        for(auto it = edges.begin(); it != edges.end(); it = next) {
            next = std::next(it);
            Line& line = *it;
            
            if(line.maxDist2 < distance2) {
                edges.erase(it);
                continue;
            } else if(line.minDist2 > distance2) {
                bestDist2 = std::min(bestDist2, line.minDist2);
                continue;
            }
            
            for(Self *const& child : line.node->children) {
                Num dist2 = center.distanceTo2(child->center());
                if(dist2 < bestDist2 && dist2 > distance2) {
                    nextNode = child;
                    bestDist2 = dist2;
                }
            }
        }
        
        //distance2 = std::min(bestDist2, distance2);
        //if(bestDist2 != std::numeric_limits<Num>::max()) distance2 = bestDist2;
        
        if(nextNode == nullptr) {
            if(edges.empty()) this->expand();
            if(edges.empty()) {
                current = nullptr;
                return *this;//Dead end
            }
            else return this->operator++();
        }
        
        this->setCurrent(nextNode);*/
        //return *this;
    }

    MDN_TPL
    void MDNT::iterator::setCurrent(MDNT::Self * const& nxt) {
        current = nxt;
        distance2 = center.distanceTo2(current->center());
        this->expand();
    }

    MDN_TPL
    void MDNT::iterator::addNode(Self *node) {
        //std::cout << "\titer attempted to add node=" << node << "\n";
        //if (node != nullptr) std::cout << "\t\tat " << node->value.absolute << "\n";
        if (node == nullptr || visited.find(node) != visited.end()) return;
        //std::cout << "Actually adding node!!\n";
        visited.insert(node);

        /*if(node->children.empty()) return this->addNode(node->parent);
        
        Line line;
        line.maxDist2 = line.minDist2 = center.distanceTo2(node->center());
        for(Self *const& child : node->children) {
            Num dist = center.distanceTo2(child->center());
            line.maxDist2 = std::max(line.maxDist2, dist);
            line.minDist2 = std::min(line.minDist2, dist);
        }
        
        //if(line.maxDist2 < distance2) return;
        
        line.node = node;
        edges.push_back(line);*/
        Vec del = (node->center() - center).makeRelative(true);

        queued.insert(QNode{node, del.magnitude2(), 0.0});
    }

    MDN_TPL
    void MDNT::iterator::expand() {
        if (parent == nullptr) {
            current = nullptr;
            return;
        }

        for (Vec::AxisData& dat : parent->span) {
            VectorAxis * const& axis = dat.axis;
            if (axis == nullptr || !axes.has(axis)) continue;

            Vec queryPos;
            if (current->span.has(axis)) {
                queryPos[axis] = current->span[axis];
            } else queryPos[axis] = parent->span[axis];

            //Query just barely outside our span, so as to reach whatever precision which the target might be subdivided to
            //queryPos = (queryPos * 0.501 + current->center()).filter(center);
            //std::cout << "\tNEAREST iter expand query " << queryPos << "\n";
            this->addNode(parent->resolveMDN((queryPos * 0.501 + current->center())));
            this->addNode(parent->resolveMDN((queryPos * -0.501 + current->center())));
        }
    }

    MDN_TPL
    typename MDNT::iterator MDNT::nearest(const typename MDNT::Vector& center, const typename MDNT::Vector& axes) {
        if (!axes) return Self::iterator(this, center);
        else return Self::iterator(this, center, axes);
    }
};