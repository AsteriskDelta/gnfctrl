#include "History.h"
#include <chrono>

namespace GNFC {
    
    namespace Dbg {
        static bool OverrideTime = false;
        static double OverrideValue = 0.0;
    }

    double CurrentTime() {
        if(Dbg::OverrideTime) {
            return Dbg::OverrideValue;
        } else {
            auto now = std::chrono::system_clock::now();
            auto now_ms = std::chrono::time_point_cast<std::chrono::microseconds>(now);
            auto epoch = now_ms.time_since_epoch();
            auto value = std::chrono::duration_cast<std::chrono::microseconds>(epoch);
            return double(value.count())/(1000.0*1000);
        }
    }
    
    void OverrideTime() {
        Dbg::OverrideTime = true;
        Dbg::OverrideValue = CurrentTime();
    }
    void StepTime(double dT) {
        Dbg::OverrideValue += dT;
    }
    /*History::History() {
    }

    History::History(const History& orig) {
    }

    History::~History() {
    }*/

};