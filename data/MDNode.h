#ifndef MDNODE_H
#define MDNODE_H
#include "GNFInclude.h"
#include <vector>
#include <array>
#include <fstream>
#include <list>
#include <unordered_set>
#include <set>
#include <mutex>

namespace GNFC {

    inline constexpr unsigned int SplitRate = 3;
    
    template<typename VEC, typename VAL>
    class MDNode {
        //friend class Directive;
    public:
        typedef VAL Value;
        typedef VEC Vector;
        typedef typename VEC::Value VectorValue;
        typedef typename VEC::AxisType VectorAxis;
        typedef MDNode<VEC,VAL> Self;
        
        struct iterator;
        
        //typedef Vector(*)(Vector&) ResolveFN;
        
        struct Weighted {
            VectorValue weight;
            Self *node;
            unsigned int depth;
            bool hasInfluence;
        };
        struct Set : std::vector<Weighted> {
            
        };
        
        MDNode();
        //MDNode(const MDNode& orig);
        ~MDNode();
        
        Self* insert(const Vector& state, const Vector& record, const VectorValue& weight);
        Self* assimilate(const Vector& state, const Vector& record, const VectorValue& weight, const bool& leaf);
        
        Vector query(const Vector& state, unsigned int *rDepth = nullptr, Num *rDist = nullptr/*, ResolveFN resolver= &(Value::operator Vector())*/);
        
        Vector sample(const Vector& state, Vector axes = Vector());
        
        iterator nearest(const Vector& center, const Vector& axes = Vector());
        
        bool contains(const Vector& state);
        bool consistant(const Vector& state);//Should it be encorporated (doesn't exceed local variance threshold for split)
        
        std::string iden() const;
        void debugGraph(std::fstream& stream, bool root=true);
        
        Vector origin, span;
        Value value;
        
        inline Vector near() const {
            return origin;
        }
        inline Vector far() const {
            return origin+span;
        }
        
        std::vector<Self*> children;
        Self *parent;
        
        inline const Vec& center() const {
            return value.absolute;
        }
        
        inline Self* root() const {
            if(parent == nullptr) return const_cast<Self*>(this);
            else return parent->root();
        }
        
        struct iterator {//Nearest to center
            iterator(Self *par, const Vec& cen, const Vec& axes = Vec());
            
            Value* operator->();
            inline Value& operator*() {
                return *(this->operator->());
            }
            iterator& operator++();
            inline operator bool() const {
                return current != nullptr;
            }
            
            inline Num distance() const {
                return sqrt(distance2);
            }
            
            Self* parent, *current, *start;
            Vector center, axes;
            Num distance2;
            
            void setCurrent(Self *const& nxt);
            
            struct QNode {
                Self *node;
                Num minDist2, maxDist2;
                inline bool operator<(const QNode& o) const {
                    return minDist2 > o.minDist2;
                }
            };
            //std::list<Line> edges;
            std::unordered_set<Self*> visited;
            std::set<QNode> queued;
            
            void addNode(Self *node);
            void expand();
        };
        
        //protected:
        
        void split(const Vector& axes);
        
        Set resolve(const Vector& state);
        Set& resolve(const Vector& state, Set& set);
        Self* resolveMDN(const Vector& state); 
    protected:
        unsigned int _limiter;
        bool differentiated;
        bool indivisible;
        unsigned int llID;
        
        std::array<VectorAxis*, SplitRate> splits;
        
        std::mutex modMtx;
    };
};

#endif /* MDNODE_H */

