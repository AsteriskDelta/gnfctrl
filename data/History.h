#ifndef HISTORY_H
#define HISTORY_H
#include "GNFInclude.h"
#include <list>

namespace GNFC {
    double CurrentTime();
    void OverrideTime();
    void StepTime(double dT);
    
    

    template<typename VALUE, typename TIME = double>
    class History {
    public:
        inline History() : duration(1000.0), maxSamples(4096), running(), denom(0.0), data() {
            
        }
        inline History(const History& orig) : duration(orig.duration), running(orig.running), denom(orig.denom), data(orig.data)  {
            
        }
        
        struct DP {
            TIME time;
            VALUE value;
            
            TIME duration = 0.0;
            Num weight = 1.0;
            
            inline TIME start() const {
                return time - duration;
            }
            inline TIME end() const {
                return time + duration;
            }
        };
        TIME duration;
        unsigned int maxSamples;
        
        inline void setDuration(const TIME& len) {
            duration = len;
            this->cull();
        }
        inline void setMaxSamples(unsigned int maxSmp) {
            maxSamples = maxSmp;
            this->cull();
        }
        
        inline void cull() {
            //TIME cullTime = CurrentTime() - duration;
            //while(!data.empty() && data.front().time < cullTime) data.pop_front();
            while(data.size() > maxSamples) data.pop_front();
        }
        
        inline void add(const VALUE& val, const Num& weight = 1.0) {
            data.push_back(DP{CurrentTime(), val});
            this->account(std::prev(data.end()), weight);
            this->cull();
        }
        
        inline VALUE dt(const TIME& len, TIME start = 0.0) const {
            if(start == 0.0) start = CurrentTime() - len;
            TIME end = start + len;
            VALUE ret = 0.0;
            TIME totalDuration = 0.0;
            for(auto it = std::next(data.begin()); it != data.end(); ++it) {
                auto prev = std::prev(it);
                if(it->time > start && it->time < end) {
                    TIME sampleDuration = it->duration;//(it->duration + prev->duration) / 2.0;
                    ret += (it->value - prev->value);// * sampleDuration;
                    totalDuration += sampleDuration;
                }
            }
            if(totalDuration == 0.0) return 0.0;
            return ret / totalDuration;
        }
        inline VALUE integ(const TIME& len, TIME start = 0.0) const {
            TIME end;
            if(start == 0.0) {
                start = CurrentTime() - len;
                end = std::numeric_limits<TIME>::max();
            } else end = start + len;
            
            VALUE ret = 0.0;
            TIME totalDuration = 0.0;
            for(auto it = data.begin(); it != data.end(); ++it) {
                if(it->time > start && it->time <= end) {
                    ret += (it->value) * it->duration;
                    totalDuration += it->duration;
                    //std::cout << "\t\t\t\tinteg incorp " << it->value << " * " << it->duration << " = " << ((it->value) * it->duration) << "\n";
                }
            }
            if(totalDuration == 0) return ret;
            return ret / totalDuration;
        }
        inline VALUE mean(const TIME& len, TIME start = 0.0) const {
            return integ(len, start);
        }
        inline VALUE variance(const TIME& len, TIME start = 0.0) const {
            if(start == 0.0) start = CurrentTime() - len;
            TIME end = start + len;
            VALUE ret = 0.0;
            TIME totalDuration = 0.0;
            VALUE mid = this->mean(len, start);
            
            for(auto it = data.begin(); it != data.end(); ++it) {
                if(it->time > start && it->time < end) {
                    VALUE tmp = (it->value - mid);
                    //if(tmp >= 0) tmp = tmp*tmp;
                    //else tmp = -(tmp*tmp);
                    tmp = tmp * tmp;
                    //std::cout << "\t\t\tvar calc sq of " << it->value << " - " << mid << " = " << tmp << "\n";
                    ret += tmp * it->duration;
                    totalDuration += it->duration;
                }
            }
            //std::cout << "\t\tvar div by " << (mid * totalDuration) << "\n";
            if(mid*totalDuration == 0.0) return 0.0;
            return ret / (totalDuration);
        }
        
        inline VALUE range(const TIME& len, TIME start = 0.0) const {
            TIME end;
            if(start == 0.0) {
                start = CurrentTime() - len;
                end = std::numeric_limits<TIME>::max();
            } else end = start + len;
            
            VALUE min = std::numeric_limits<VALUE>::max(), max = std::numeric_limits<VALUE>::lowest();
            for(auto it = data.begin(); it != data.end(); ++it) {
                if(it->time > start && it->time <= end) {
                    min = $::min(min, it->value);
                    max = $::max(max, it->value);
                }
            }
            //std::cout << "\t\trange min=" << min << ", max=" << max << "\n";
            if(max < min) return 0.0;
            else return max - min;
        }
        
        inline unsigned int samples() const {
            return data.size();
        }
        
    private:
        struct {
            VALUE mean = 0.0;
            VALUE ddt = 0.0;
            VALUE integ = 0.0;
        } running;
        
        TIME denom;
        
        std::list<DP> data;
        
        inline void account(typename decltype(data)::iterator iter, double mult) {
            //_unused(mult);
            DP *const cur = &(*iter);
            if(iter != data.begin()) {
                DP *const prev = &(*std::prev(iter));
                TIME dT = cur->time - prev->time;
                
                cur->duration += dT / 2.0;
                prev->duration += dT / 2.0;
                
                cur->weight = mult;
                //std::cout << "\taccounted for " << prev->value << " over " << prev->duration << "\n";
                
                denom = std::min(duration, denom + dT);
                
                //Assign so as to mimic less influence on final value
                prev->duration *= prev->weight;
            }
            
        }
        
        
        //unsigned int opDriftCnt;
    };
};

#endif /* HISTORY_H */

