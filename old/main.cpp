#include "GNFC.h"
#include "testing/Cryo.h"
#include <ARKE/Image.h>
#include <ARKE/AppData.h>
#include <ARKE/ImageFont.h>
#include "MDNode.h"
//#define IMG
using namespace GNFC;

int main(int argc, char** argv) {
    _unused(argc, argv);
    AppData::Initialize("./");
    ImageFont::Initialize();
    
    Space *space = new Space();
    Heater *heater = new Heater(); Cooler *cooler = new Cooler();
    Pump *pump = new Pump();
    
    Therm *temp = new Therm();
    Pressure *pressure = new Pressure();
    
    auto *hdi = new MDNode<Vec, RecordedJacobian>();
    space->hdi = hdi;
    
    space->add(heater);
    space->add(cooler);
    space->add(temp);
    space->add(pump);
    space->add(pressure);
    
    Vec target;
    target[temp] = 5.0;
    //target[pressure] = 8.0;
    space->setTarget(target);
    
    std::cout << "SET TARGET TO " << target << "\n";
    
    Jacobian cp;
    cp[temp] = 1.0;
    cp.absolute[heater] = 2.0;
    cp.absolute[cooler] = 0.0;
    cp.absolute[pressure] = 1.0;
    space->controls.push_back(cp);
    
    cp.clear();
    cp[temp] = -1.0;
    cp.absolute[cooler] = 20.0;
    cp.absolute[heater] = 0.0;
    cp.absolute[pressure] = 1.0;
    space->controls.push_back(cp);
    
    cp.clear();
    cp[temp] = 0.0;
    cp.absolute[cooler] = 0.0;
    cp.absolute[heater] = 0.0;
    cp.absolute[pressure] = 1.0;
    space->controls.push_back(cp);
    
    cp.clear();
    cp[pressure] = 8.0;
    cp.absolute[pump] = 10.0;
    space->controls.push_back(cp);
    
    cp.clear();
    cp[pressure] = 0.0;
    cp.absolute[pump] = 0.0;
    space->controls.push_back(cp);
    
    cp.clear();
    cp[pressure] = -8.0;
    cp.absolute[pump] = -10.0;
    space->controls.push_back(cp);
    
    double lastTime = CurrentTime();
    
    
    std::vector<Vec> stPath;
#ifdef IMG
    std::vector<ColorRGBA> axisCols;
    axisCols.resize(space->axes.size());
    for(unsigned int i = 0; i < axisCols.size(); i++) axisCols[i] = ColorRGBA(rand(), rand(), rand(), 255);
    
    ImageRGBA *img = new ImageRGBA(768, 512);
    img->clear(ColorRGBA(0,0,0,0));
    for(unsigned int x = 0; x < img->width; x++) {
        for(unsigned int y = 0; y < 64; y++) {
            img->setPixel(x,y,ColorRGBA(255));
        }
    }
    
    
    ImageRGBA *chartz = new ImageRGBA(300,300);
    
    unsigned int rx = 0;
    std::vector<unsigned int> prevY, prevDY;
    prevY.resize(axisCols.size());
    prevDY.resize(axisCols.size());
    
    ImageFont font("fnt.ttf", 28);

    while(rx < img->width) {
        /*CryoSim(0.025f);
     * */
#else
    while(true) {
#endif
        const Num dT = CurrentTime() - lastTime;
        CryoSim(dT);
        std::cout << "updating with dT = " << (dT) << "\n";
        lastTime = CurrentTime();

        space->update(dT);
        stPath.push_back(space->current);
        std::cout << "Inserting at " << space->current << " val " << space->deriv.filter(Vec::Invariant) << " with dT=" << dT << "\n";
        
        space->debugPrint();
//#ifndef IMG
        usleep(1000*50);
//#endif
#ifdef IMG
        if(rand()%40 == 0) {
            target[temp] = double(rand() % 25000)/100.0;
            target[pressure] = double(rand() % 500)/100.0;
            space->setTarget(target);
        }
#endif
        
        char line[256];
        _unused(line);
        //std::cin.getline(line, 256);
#ifdef IMG
        std::cout << "\trx = " << rx << "\n";
        unsigned int axisID = 0;
        for(auto& axisPtr : space->axes) {
            for(int i = 0; i < 2; i++) {
                unsigned int yc;
                decltype(prevY)& prevs = (i == 0)? prevY : prevDY;
                
                if(i == 0) yc = (img->height-1) - round(axisPtr->range.normalize(space->current[axisPtr]) * (img->height-1));
                else yc = round(axisPtr->dt(1.0) * 4000.0) + (img->height/2);
                ColorRGBA& col = axisCols[axisID];
                if(rx > 0) img->drawLine(rx - 1, prevs[axisID], rx, yc, col);
                else {
                    unsigned int titleBaseX = 60 + axisID * 160, titleBaseY = 8;
                    font.write(img, titleBaseX, titleBaseY, axisPtr->name);
                    img->drawPoint(titleBaseX - 16, titleBaseY + 20, col, 12);
                }
                prevs[axisID] = yc;
            }
            axisID++;
        }
        rx++;
#endif
    }
    
#ifdef IMG
    
    for(unsigned int ctemp = 0; ctemp < 300; ctemp++) {
        for(unsigned int cpressure = 0; cpressure < 300; cpressure++) {
            ColorRGBA col;
            Vec st;
            st[temp] = double(ctemp);
            st[pressure] = double(cpressure)/10.0;
            Vec del = hdi->query(st);
            std::cout << "query " << ctemp << ", " << cpressure << " got " << del << "\n";
            
            col.r = round(16.0*del[temp]) + 127;
            col.g = round(16.0*del[pressure]) + 127;
            col.b = 0;//round(16.0*del[cooler]);
            col.alphaRef() = 255;
            
            chartz->setPixel(299-ctemp, 299-cpressure, col);
        }
    }    
    img->repAlpha(0, 255);
    img->Save("graph.png");
    
    for(auto it = std::next(stPath.begin()); it != stPath.end(); ++it) {
        Vec &st = *it, &prev = *std::prev(it);
        std::cout << "drawing from " << prev << " to " << st << "\n";
        chartz->drawLine(299-prev[temp], 299-prev[pressure]*10, 299-st[temp], 299-st[pressure]*10, ColorRGBA(0,0,0,255));
    }
    
    chartz->Save("char.png");
#endif
    return 0;
}
