#ifndef USERCONTROL_H
#define USERCONTROL_H
#include "GNFInclude.h"
#include "Point.h"
#include "Directive.h"

namespace GNFC {

    class UserControl : public Directive {
    public:
        UserControl(Controller *spc);
        virtual ~UserControl();
        
        virtual void execute() override;
        
        virtual bool triggered() override;
        
        inline Vec& target() {
            return targetVec;
        }
        inline bool setTarget(const Vec& vec) {
            targetVec = vec;
            return true;
        }
    private:
        Vec targetVec;
    };
};

#endif /* USERCONTROL_H */

