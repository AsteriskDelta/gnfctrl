#include "UserControl.h"
#include "Space.h"

namespace GNFC {

    UserControl::UserControl(Controller *spc) : Directive(spc), targetVec() {
        this->priority = -100.0;
        this->name() = "UserControl";
    }

    UserControl::~UserControl() {
    }

    void UserControl::execute() {
        //Vec& spTarget = Directive::space()->target;
        //spTarget = spTarget.filter(targetVec, false) | targetVec;
        this->action.clear();
        this->action.add(targetVec);
        Directive::execute();
    }
    
    bool UserControl::triggered() {
        activation = 1.0;
        return true;
    }
};
