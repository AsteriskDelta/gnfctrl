#ifndef EXPLORE_H
#define EXPLORE_H
#include "GNFInclude.h"
#include "Point.h"
#include "Directive.h"

namespace GNFC {

    class Explore : public Directive {
    public:
        Explore(Controller *spc, Axis *pathErr);
        virtual ~Explore();
        
        virtual void execute() override;
        virtual void invoked() override;
        
        Axis *pathError;
    private:
    };
};

#endif /* EXPLORE_H */

