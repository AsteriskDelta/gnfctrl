#include "Explore.h"
#include "MDNode.h"
#include "Space.h"
#include "Controller.h"
#include "UserControl.h"
#include <fstream>

namespace GNFC {

    Explore::Explore(Controller *spc, Axis *pathErr) : Directive(spc), pathError(pathErr) {
        this->priority = 0.0;
        
        Vec minErr, maxErr;
        minErr[pathError] = 0.4;
        maxErr[pathError] = 50.0;
        
        this->name() = "Explore";
        this->current.setRange(minErr, maxErr);
    }

    Explore::~Explore() {
    }

    void Explore::execute() {
        Directive::execute();
    }
    
    void Explore::invoked() {
        std::cout << "INVOKE EXPLORE\n";
        //Pause();
        this->action.clear();
        //auto *node = space()->hdi->resolveMDN(controller->user()->target());
        auto *node = space()->hdi->resolveMDN(controller->space->current);
        Axis *modAxis = nullptr;
        auto *modNode = node;
        Num minVariance = std::numeric_limits<Num>::max();
        
        Vec sumVariance;
        Vec sumSpan, sumDenom, sumCenter;
        
        while(node != nullptr) {
            //Find the axis with the lowest absolute (positional) variance, indicating that we haven't explored its effects
            Vec nodeVariance = (node->value.absoluteHistory.variance(2000.0)).filter(Vec::Hidden, false).filter(Vec::Variant);
            //nodeVariance.makeRelative(true);//Gives 0...1 values for the value vs abs range of its axis
            //std::cout << "calc node span\n";
            Vec nodeSpan = node->span.filter(nodeVariance);
            //nodeSpan.makeRelative(true);
            std::cout << "node spans " << nodeSpan << " children=" << node->children.size() << " from raw=" << node->span.filter(nodeVariance) << "\n";
            std::cout << "exploring by pos variance = " << nodeVariance << " * " << nodeSpan << "\n";

            for(const Vec::AxisData& dat : nodeVariance) {
                Axis *axis = dat.axis;
                if(!axis->variant()) continue;

                Num effVariance = (dat.value) / ((axis->range()*axis->range()));// * (nodeSpan[axis] / axis->range());
                if(/*effVariance < minVariance && */axis->canSubdivide(node->span[axis])) {
                    //modAxis = axis;
                    //minVariance = effVariance;
                    //modNode = node;
                    sumVariance[axis] += effVariance;
                    Num mult = 1.0 / (1.0 + effVariance);
                    mult *= (nodeSpan[axis] / axis->range());
                    sumSpan[axis] += node->span[axis] * mult;// / (nodeSpan[axis] / axis->range());
                    sumCenter[axis] += node->center()[axis] * mult;
                    sumDenom[axis] += mult;
                }
            }
            node = node->parent;
        }
        
        Vec exploreCenter, exploreOffset;
        for(const Vec::AxisData& dat : sumVariance) {
            std::cout << "\tcmp for " << dat.axis->name << " " << dat.value << " <? " << minVariance << ", trying span " << (sumSpan[dat.axis] / sumDenom[dat.axis]/2) << "\n";
            if(dat.value < minVariance && dat.axis->canSubdivide(sumSpan[dat.axis] / sumDenom[dat.axis])) {
                modAxis = dat.axis;
                minVariance = dat.value;
                exploreCenter.clear();
                exploreOffset.clear();
                exploreCenter = controller->space->current;
                exploreCenter[dat.axis] = sumCenter[dat.axis] / sumDenom[dat.axis];
                exploreOffset[dat.axis] = sumSpan[dat.axis] / sumDenom[dat.axis] / 2.0;
            }
        }
        
        //if(modAxis == nullptr || modNode == nullptr) return;
        node = modNode;
        
        Vec modAxes;
        modAxes.touch(modAxis);
        //bool negate = (rand() % 2) == 1;
        /*Vec exploreOffset = (node->span.filter(modAxes) / (2.001));
        */
        Vec currentOffset = space()->current - exploreCenter;//node->center();
        if(/*currentOffset.distanceTo(exploreOffset) < currentOffset.distanceTo(exploreOffset * -1.0)*/rand()%2 == 1) exploreOffset *= -1.0;
        Vec explorePos = (exploreCenter + exploreOffset).filter(Vec::Variant);//node->center().filter(modAxes) + exploreOffset;
        explorePos.clamp();
        std::cout << "exploring from " << exploreCenter << " + " << exploreOffset << " = " << explorePos << "\n";
            //Pause();
        //if(space()->current.distanceTo(explorePos) < space()->current.distanceTo(explorePos * -1.0)) {//Negate to be as far away from current state as possible
        //    explorePos = explorePos * -1.0;
        //}
        //explorePos |= controller->current();
        
        //std::cout << "exploring axis " << modAxis->name << " to vec " << explorePos << "\n";
        char line[256];
        _unused(line);
        
        
        
        //std::cin.getline(line, 256);
        //Vec seqTarget;
        //seqTarget[modAxis] = explorePos;
        this->action.clear();
        Sequence::Step *step = this->action.add(explorePos);
        step->instant = true;
        step->holdTime = 2.0;
    }
};