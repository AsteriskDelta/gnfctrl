#include <iostream>
#include <cstdlib>
#include "GNFC.h"
#include <ARKE/Image.h>
#include <ARKE/AppData.h>
#include <ARKE/ImageFont.h>
#include <signal.h>

#include "History.h"

#include "testing/Cryo.h"

Controller *controller = new Controller();

Heater *heater = new Heater();
Cooler *cooler = new Cooler();
Pump *pump = new Pump();

Therm *temp = new Therm();
Pressure *pressure = new Pressure();

static bool paused = false;
namespace GNFC {

    void Pause() {
        paused = true;
        static char line[256];
        std::cin.getline(line, 256);
        std::cout << "GOT LINE " << line << "\n";
        if (line[0] == 'g') {
            Vec axes;
            axes[heater] = 1;
            axes[cooler] = 1;
            std::cout << "MAKING IMG " << heater->range() << "x" << cooler->range() << "\n";
            ImageRGBA *img = new ImageRGBA(heater->range()*10, cooler->range()*10);
            for (unsigned int x = 0; x < img->width; x++) {
                for (unsigned int y = 0; y < img->height; y++) {
                    ColorRGBA col(0, 0, 0, 255);

                    Vec pt;
                    pt[heater] = x/10.0 - heater->range.min;
                    pt[cooler] = y/10.0 - cooler->range.min;
                    Vec sample = controller->hdi->query(pt);

                    col.r = col.g = col.b = 127 + (127.0 * sample[temp] / 8.0);

                    img->setPixel(x, y, col);
                }
            }
            img->Save("response.png");
            delete img;
        } else if(line[0] == 'c') {
            static unsigned int expSeq = 0;
            std::string dotFName = "dbg/" + std::to_string(expSeq) + ".dot";
            std::string dotImgName = "dbg/" + std::to_string(expSeq++) + ".png";
            std::fstream fs;
            fs.open (dotFName, std::fstream::in | std::fstream::out | std::fstream::trunc);
            controller->hdi->debugGraph(fs);
            fs.close();
            std::string cmd = "dot -Tpng "+dotFName+" -o " + dotImgName;
            system(cmd.c_str());
        } else if(line[0] == 'x') {
            exit(0);
        }
        
        std::cout << ">>>END INTERRUPT\n";
        
        paused = false;
    }
};

void sigINT(int sig) {
    sigset_t ss;
    sigemptyset(&ss);
    sigaddset(&ss, sig);
    sigprocmask(SIG_UNBLOCK, &ss, NULL);
    //pthread_sigmask(SIG_UNBLOCK, &ss, NULL);
    
    if(paused) exit(0);
    else GNFC::Pause();
}

int main(int argc, char** argv) {
    _unused(argc, argv);
    AppData::Initialize("./");
    ImageFont::Initialize();

    OverrideTime();

    signal(SIGINT, sigINT);

    controller->addAxis(heater);
    controller->addAxis(cooler);
    controller->addAxis(temp);
    controller->addAxis(pump);
    controller->addAxis(pressure);

    double lastTime = CurrentTime();

    History<Vec, Num> history;
    history.setDuration(60.0 * 10);

    Vec target;
    target[temp] = 42.5;
    controller->user()->setTarget(target);

    while (true) {
        const Num dT = CurrentTime() - lastTime;
        CryoSim(dT);
        controller->update(dT);
        //std::cout << "updating with dT = " << (dT) << "\n";
        lastTime = CurrentTime();

        history.add(controller->current());

        controller->debugPrint();
        controller->space->debugPrint();
        std::cout << "\n";
        //usleep(1000*50);

        char line[256];
        _unused(line);
        //std::cin.getline(line, 256);
        StepTime(0.1);
    }
    return 0;
}

